//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ostream>
#include <time.h>
#include "TLibCommon/CommonDef.h"
#include "TLibDepthEstimation/paramsDE.h"

#include "TLibDepthEstimation/cCfgParams.h"
#include "TLibCommon/cCamParams.h"
#include "TLibCommon/cYUV.h"
#include "TLibCommon/cArray.h"

#include "TLibDepthEstimation/cSADPixelMatcher.h"
#include "TLibDepthEstimation/cSADBlockMatcher.h"
#include "TLibDepthEstimation/cLocalizer.h"
#include "TLibDepthEstimation/cGraphCutSPOptimizer.h"

#include "TLibSegmentation/cSegmentation.h"
#include "TLibSegmentation/cSNICSuperpixel.h"

int main(Int argc, Char* argv[])
{

	// print information
	fprintf(stdout, "\n");
	fprintf(stdout, "Poznan University of Technology Depth Estimation Software");
	fprintf(stdout, PUT_ONOS);
	fprintf(stdout, PUT_COMPILEDBY);
	fprintf(stdout, PUT_BITS);
	fprintf(stdout, "\n");

	// starting time
	clock_t clkTimeBegin, clkTimeEnd;
	clkTimeBegin = clock();

	Char cfgName[MAX_TEXT_LENGTH];

	if (argc < 2) {
		fprintf(stdout, "Configuration file was not provided\n");
		system("PAUSE");
		return 0;
	}

	strcpy_s(cfgName, MAX_TEXT_LENGTH, argv[1]);

	// configuration parameters
	cCfgParams cfgParams(cfgName);

	// camera parameters
	cArray<cCamParams<MatrixComputationalType>*> apcCameraParameters;
	readParamsFromFile(cfgParams.m_sFileCameraParameter, apcCameraParameters, cfgParams.m_acCameraNames);

	// input yuvs init
	cArray<cYUV<ImagePixelType>*> apcYUVinput; apcYUVinput.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cYUV<ImagePixelType>* pcYUV = new cYUV<ImagePixelType>(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, VIDEO_BPS, cfgParams.m_uiChrominanceFormat); apcYUVinput.push_back(pcYUV);
	}

	cArray<cYUV<ImagePixelType>*> apcYUVprevinput;
	if (cfgParams.m_uiTotalNumberOfFrames > 1) {
		apcYUVprevinput.reserve(cfgParams.m_uiNumberOfCameras);
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			cYUV<ImagePixelType>* pcYUV = new cYUV<ImagePixelType>(cfgParams.m_uiSourceWidth*cfgParams.m_uiHorizontalPrecision, cfgParams.m_uiSourceHeight*cfgParams.m_uiVerticalPrecision, VIDEO_BPS, cfgParams.m_uiChrominanceFormat); apcYUVprevinput.push_back(pcYUV);
		}
	}

	//output yuvs init
	cArray<cYUV<DepthPixelType>*> apcYUVdepth;	apcYUVdepth.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cYUV<DepthPixelType>* pcYUVDepth = new cYUV<DepthPixelType>(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, DEPTH_BPS, 400); apcYUVdepth.push_back(pcYUVDepth);
	}
	cYUV<DepthPixelType>* pcYUVDepth = new cYUV<DepthPixelType>(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, DEPTH_BPS, 400);

	cArray<cYUV<ImagePixelType>*> apcYUVsp;

	apcYUVsp.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cYUV<ImagePixelType>* pcYUV = new cYUV<ImagePixelType>(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, VIDEO_BPS, cfgParams.m_uiChrominanceFormat); apcYUVsp.push_back(pcYUV);
	}


	//output depth maps
	cArray<UInt*> apuiDepthLabel; apuiDepthLabel.reserve(cfgParams.m_uiNumberOfCameras);
	UInt uiImSize = cfgParams.m_uiSourceWidth * cfgParams.m_uiSourceHeight;
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		UInt* puiDepthLabel = new UInt[uiImSize]; apuiDepthLabel.push_back(puiDepthLabel);
	}

	//for temporal enhancement
	cArray<UInt*> apuiPrevDepthLabel;
	if (cfgParams.m_uiTemporalEnhancementMethod > 0) {
		apuiPrevDepthLabel.reserve(cfgParams.m_uiNumberOfCameras);
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			UInt* puiPrevDepthLabel = new UInt[uiImSize]; apuiPrevDepthLabel.push_back(puiPrevDepthLabel);
		}
	}

	cArray<UInt*> apuiIDepthLabel;
	if (cfgParams.m_uiTemporalEnhancementMethod > 0) {
		apuiIDepthLabel.reserve(cfgParams.m_uiNumberOfCameras);
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			UInt* puiIDepthLabel = new UInt[uiImSize]; apuiIDepthLabel.push_back(puiIDepthLabel);
		}
	}

	cArray<UInt*> apuiPrevSPLabel;
	cArray<UInt*> apuiISPLabel;

	apuiPrevSPLabel.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		UInt* pcYUV = new UInt[uiImSize]; apuiPrevSPLabel.push_back(pcYUV);
	}

	apuiPrevSPLabel.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		UInt* pcYUV = new UInt[uiImSize]; apuiISPLabel.push_back(pcYUV);
	}


	cMatcher *matcher;

	switch (cfgParams.m_iMatchingMethod) {

	case 0:
		if (cfgParams.m_uiMatchingBlockWidth == 1 && cfgParams.m_uiMatchingBlockHeight == 1)
			matcher = new cSADPixelMatcher;
		else
			matcher = new cSADBlockMatcher;
		break;
	default:
		matcher = new cSADPixelMatcher;
	}
	cOptimizer *optimizer = new cGraphCutSPOptimizer;
	cLocalizer *localizer = new cLocalizer;

	//matcher init
	matcher->init(cfgParams);
	localizer->init(apcCameraParameters, cfgParams);

	if (cfgParams.m_uiTemporalEnhancementMethod > 0) {
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			memset(apuiPrevDepthLabel[uiCamId], 0, uiImSize * sizeof(UInt));
		}
	}

	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		memset(apuiPrevSPLabel[uiCamId], 0, uiImSize * sizeof(UInt));
	}

	//segmentation 
	cArray<cSegmentation*> apcSegmentations;
	cArray<cSegmentation*> apcPrevSegmentations;
	cArray<cSegmentation*> apcISegmentations;

	apcSegmentations.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cSegmentation* pcSegmentation;

		pcSegmentation = new cSNICSuperpixel;

		pcSegmentation->init(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, cfgParams.m_dSuperpixelColorCoeff, cfgParams.m_uiNumOfSuperpixels);
		apcSegmentations.push_back(pcSegmentation);
	}

	apcPrevSegmentations.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cSegmentation* pcSegmentation;
		pcSegmentation = new cSNICSuperpixel;
		pcSegmentation->init(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, cfgParams.m_dSuperpixelColorCoeff, cfgParams.m_uiNumOfSuperpixels);
		apcPrevSegmentations.push_back(pcSegmentation);
	}

	apcISegmentations.reserve(cfgParams.m_uiNumberOfCameras);
	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
		cSegmentation* pcSegmentation;
		pcSegmentation = new cSNICSuperpixel;
		pcSegmentation->init(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, cfgParams.m_dSuperpixelColorCoeff, cfgParams.m_uiNumOfSuperpixels);
		apcISegmentations.push_back(pcSegmentation);
	}

	//===========================================
	// Loop for every frame
	//===========================================
	for (UInt uiFrameId = cfgParams.m_uiStartFrame; uiFrameId < cfgParams.m_uiTotalNumberOfFrames + cfgParams.m_uiStartFrame; uiFrameId++) {
		fprintf(stdout, "\nFrame %d\n", uiFrameId);

		//yuv frame reading
#pragma omp parallel for num_threads(cfgParams.m_uiNumOfThreads)
		for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			if ((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) {
				for (UInt pp = 0; pp < uiImSize*cfgParams.m_uiHorizontalPrecision*cfgParams.m_uiVerticalPrecision; pp++) {
					apcYUVprevinput[uiCamId]->m_atY[pp] = apcYUVinput[uiCamId]->m_atY[pp];
				}
				for (UInt pp = 0; pp < uiImSize / 4; pp++) {
					apcYUVprevinput[uiCamId]->m_atU[pp] = apcYUVinput[uiCamId]->m_atU[pp];
					apcYUVprevinput[uiCamId]->m_atV[pp] = apcYUVinput[uiCamId]->m_atV[pp];
				}
			}
			apcYUVinput[uiCamId]->frameReader(cfgParams.getFileInputView(uiCamId), uiFrameId);

			for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
				apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp].clear();
			}
			apcSegmentations[uiCamId]->m_apiSuperpixel.clear();
			apcSegmentations[uiCamId]->init(cfgParams.m_uiSourceWidth, cfgParams.m_uiSourceHeight, cfgParams.m_dSuperpixelColorCoeff, cfgParams.m_uiNumOfSuperpixels);
			apcSegmentations[uiCamId]->compute(*apcYUVinput[uiCamId], uiFrameId, apuiPrevSPLabel[uiCamId], cfgParams.m_uiTemporalEnhancementMethod);
			apcSegmentations[uiCamId]->compute_neigh();
		}

		for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			for (UInt pp = 0; pp < uiImSize; pp++) {
				if (apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp] == apcSegmentations[uiCamId]->m_uiNumOfSuperpix) apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp] = 0;
			}
		}


		if (cfgParams.m_iMatchingMethod == 2)matcher->precomputeMatchingErrors(apcYUVinput, apcSegmentations);
		//optimizer init
		optimizer->init(cfgParams, matcher, apcSegmentations);

		if ((cfgParams.m_uiTemporalEnhancementMethod == 1)) {
			if ((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) {
				for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
					UInt cnt = 0;
					for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
						UInt uiSY = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4];
						UInt uiSX = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3];


						if (apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX] < apcPrevSegmentations[uiCamId]->m_uiNumOfSuperpix) {
							Double dThr = cfgParams.m_dTempThresh;
							if ((Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0] - apcPrevSegmentations[uiCamId]->m_apiSuperpixel[apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][0]) > dThr) ||
								(Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1] - apcPrevSegmentations[uiCamId]->m_apiSuperpixel[apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][1]) > dThr) ||
								(Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2] - apcPrevSegmentations[uiCamId]->m_apiSuperpixel[apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][2]) > dThr)
								)
							{
								cnt++;
								apuiPrevDepthLabel[uiCamId][uiSPp] = apuiDepthLabel[uiCamId][apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]] + cfgParams.m_uiNumberOfDepthSteps;
							}

							else apuiPrevDepthLabel[uiCamId][uiSPp] = apuiDepthLabel[uiCamId][apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]];
						}
						else apuiPrevDepthLabel[uiCamId][uiSPp] = apuiDepthLabel[uiCamId][apuiPrevSPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]] + cfgParams.m_uiNumberOfDepthSteps;

					}
				}

			}
		}

		//optimization
		optimizer->optimize(apcYUVinput, apcCameraParameters, apuiDepthLabel, apuiPrevDepthLabel, apcYUVprevinput, apcSegmentations, uiFrameId);
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
				apcSegmentations[uiCamId]->Superpixel_neigh[uiSPp].clear();
			}
			apcSegmentations[uiCamId]->Superpixel_neigh.clear();
		}

		if ((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) {
#pragma omp parallel for num_threads(cfgParams.m_uiNumOfThreads)
			//*
			for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
				UInt cnt = 0;
				for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
					UInt uiSY = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4];
					UInt uiSX = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3];

					if (apuiISPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX] < apcPrevSegmentations[uiCamId]->m_uiNumOfSuperpix) {
						Double dThr = cfgParams.m_dTempThresh;
						if ((Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0] - apcISegmentations[uiCamId]->m_apiSuperpixel[apuiISPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][0]) < dThr) &&
							(Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1] - apcISegmentations[uiCamId]->m_apiSuperpixel[apuiISPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][1]) < dThr) &&
							(Abs(apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2] - apcISegmentations[uiCamId]->m_apiSuperpixel[apuiISPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]][2]) < dThr)
							)
						{
							apuiDepthLabel[uiCamId][uiSPp] = apuiIDepthLabel[uiCamId][apuiISPLabel[uiCamId][uiSY*cfgParams.m_uiSourceWidth + uiSX]];
						}
					}

				}
			}

		}
		else {
			for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
				for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
					apuiIDepthLabel[uiCamId][uiSPp] = apuiDepthLabel[uiCamId][uiSPp];
				}
			}
		}

		optimizer->calcDepth(apcYUVdepth, apuiDepthLabel, apcSegmentations);
		for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
			//local
			localizer->localize(apcYUVdepth, uiCamId, cfgParams);
			apcYUVdepth[uiCamId]->frameWriter(cfgParams.getFileOutputDepthMap(uiCamId), (uiFrameId != cfgParams.m_uiStartFrame));

		}
		if (cfgParams.m_uiTemporalEnhancementMethod) {
			for (Int uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {
				apcPrevSegmentations[uiCamId]->m_uiNumOfSuperpix = apcSegmentations[uiCamId]->m_uiNumOfSuperpix;
				if (((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) == 0) apcISegmentations[uiCamId]->m_uiNumOfSuperpix = apcSegmentations[uiCamId]->m_uiNumOfSuperpix;
				for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
					apcPrevSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0];
					apcPrevSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1];
					apcPrevSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2];
					apcPrevSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3];
					apcPrevSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4];
				}
				if (((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) == 0) {
					for (UInt uiSPp = 0; uiSPp < apcSegmentations[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
						apcISegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][0];
						apcISegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][1];
						apcISegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][2];
						apcISegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][3];
						apcISegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4] = apcSegmentations[uiCamId]->m_apiSuperpixel[uiSPp][4];
					}
				}
				for (UInt pp = 0; pp < uiImSize; pp++) {
					if (apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp] < apcSegmentations[uiCamId]->m_uiNumOfSuperpix)apuiPrevSPLabel[uiCamId][pp] = apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp];
					else apuiPrevSPLabel[uiCamId][pp] = apcSegmentations[uiCamId]->m_uiNumOfSuperpix;
				}
				if (((uiFrameId - cfgParams.m_uiStartFrame) % cfgParams.m_uiIFramePeriod) == 0) {
					for (UInt pp = 0; pp < uiImSize; pp++) {
						if (apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp] < apcSegmentations[uiCamId]->m_uiNumOfSuperpix)apuiISPLabel[uiCamId][pp] = apcSegmentations[uiCamId]->m_cYUVlabel.m_atY[pp];
						else apuiISPLabel[uiCamId][pp] = apcSegmentations[uiCamId]->m_uiNumOfSuperpix;
					}
				}
			}
		}

	} //frame loop end

	// ending time
	clkTimeEnd = clock();
	fprintf(stdout, "\nTotal Time: %12.3f sec.\n", ((clkTimeEnd - clkTimeBegin) / 1000.0));
	//===========================================
	// Clear the memory
	//===========================================
	while (!apcYUVinput.empty()) {
		delete apcYUVinput.at(apcYUVinput.size() - 1);
		apcYUVinput.pop_back();
	}

	for (UInt uiCamId = 0; uiCamId < cfgParams.m_uiNumberOfCameras; uiCamId++) {

		delete apcYUVsp[uiCamId];
		delete apcSegmentations[uiCamId];
		delete apuiPrevSPLabel[uiCamId];
	}

	while (!apcYUVprevinput.empty()) {
		delete apcYUVprevinput.at(apcYUVprevinput.size() - 1);
		apcYUVprevinput.pop_back();
	}
	while (!apcYUVdepth.empty()) {
		delete apcYUVdepth.at(apcYUVdepth.size() - 1);
		apcYUVdepth.pop_back();
	}

	delete pcYUVDepth;

	while (!apuiDepthLabel.empty()) {
		delete apuiDepthLabel.at(apuiDepthLabel.size() - 1);
		apuiDepthLabel.pop_back();
	}

	while (!apuiPrevDepthLabel.empty()) {
		delete apuiPrevDepthLabel.at(apuiPrevDepthLabel.size() - 1);
		apuiPrevDepthLabel.pop_back();
	}

	while (!apcCameraParameters.empty()) {
		delete apcCameraParameters.at(apcCameraParameters.size() - 1);
		apcCameraParameters.pop_back();
	}

	delete optimizer;
	delete localizer;
	delete matcher;

	system("PAUSE");
	return 0;
}
