#ifndef __CSNICSUPERPIXEL_H__
#define __CSNICSUPERPIXEL_H__

#include "TLibCommon\TypeDef.h"
#include "TLibCommon\cYUV.h"
#include "TLibCommon\cArray.h"
#include "TLibDepthEstimation\paramsDE.h"
#include "cSegmentation.h"

class cSNICSuperpixel : public cSegmentation {
public:
	Void compute(cYUV<ImagePixelType> &YUV, UInt uiFrameId, UInt* &cYUVlabel, UInt uiTemporalEnhancementMethod);
	Void FindSeeds(const int width, const int height, int& numk, std::vector<int>& kx, std::vector<int>& ky);
	Void runSNIC(
		ImagePixelType*		Yv,
		ImagePixelType*		Uv,
		ImagePixelType*		Vv,
		const int					width,
		const int					height,
		int*					   labels,
		int*						outnumk,
		const int				  innumk,
		const double			   compactness);
};


#endif