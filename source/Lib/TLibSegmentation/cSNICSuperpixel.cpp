#include "cSNICSuperpixel.h"
#include <cmath>
#include <math.h>

#include "TLibCommon\TypeDef.h"
#include "TLibCommon\cYUV.h"
#include "TLibCommon\cArray.h"
#include "TLibDepthEstimation\paramsDE.h"

//=================================================================================
//  snic_mex.cpp
//
//
//  AUTORIGHTS
//  Copyright (C) 2016 Ecole Polytechnique Federale de Lausanne (EPFL), Switzerland.
//
//  Created by Radhakrishna Achanta on 05/November/16 (firstname.lastname@epfl.ch)
//
//
//  Code released for research purposes only. For commercial purposes, please
//  contact the author.
//=================================================================================
/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of EPFL nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cmath>
#include <cfloat>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

//===========================================================================
/// FindSeeds
//===========================================================================
Void cSNICSuperpixel::FindSeeds(const int width, const int height, int& numk, std::vector<int>& kx, std::vector<int>& ky)
{
	const int sz = width * height;
	int gridstep = sqrt(double(sz) / double(numk)) + 0.5;
	int halfstep = gridstep / 2;
	double h = height; double w = width;

	int xsteps = int(width / gridstep);
	int ysteps = int(height / gridstep);
	int err1 = abs(xsteps*ysteps - numk);
	int err2 = abs(int(width / (gridstep - 1))*int(height / (gridstep - 1)) - numk);
	if (err2 < err1)
	{
		gridstep -= 1.0;
		xsteps = width / (gridstep);
		ysteps = height / (gridstep);
	}

	numk = (xsteps*ysteps);
	kx.resize(numk); ky.resize(numk);
	int n = 0;
	for (int y = halfstep, rowstep = 0; y < height && n < numk; y += gridstep, rowstep++)
	{
		for (int x = halfstep; x < width && n < numk; x += gridstep)
		{
			if (y <= h - halfstep && x <= w - halfstep)
			{
				kx[n] = x;
				ky[n] = y;
				n++;
			}
		}
	}
}

Void cSNICSuperpixel::compute(cYUV<ImagePixelType> &YUV, UInt uiFrameId, UInt* &cYUVlabel, UInt uiTemporalEnhancementMethod) {

	const int w = m_uiSourceWidth;
	const int h = m_uiSourceHeight;
	const int sz = w * h;
	const int dx8[8] = { -1,  0, 1, 0, -1,  1, 1, -1 };//for 4 or 8 connectivity
	const int dy8[8] = { 0, -1, 0, 1, -1, -1, 1,  1 };//for 4 or 8 connectivity
	const int dn8[8] = { -1, -w, 1, w, -1 - w,1 - w,1 + w,-1 + w };
	const int dn8UV1[8] = { -1, 0, 1, 0, -1, 1, 1, -1 };
	const int dn8UVw[8] = { 0, -1, 0, 1, -1,-1,1,1 };


	struct NODE
	{
		unsigned int i; // the x and y values packed into one
		unsigned int k; // the label
		double d;	   // the distance
	};
	struct compare
	{
		bool operator()(const NODE& one, const NODE& two)
		{
			return one.d > two.d;//for increasing order of distances
		}
	};
	//-------------
	// Find seeds
	//-------------
	vector<int> cx(0), cy(0);
	int numk = m_uiNumOfSuperpix;
	FindSeeds(w, h, numk, cx, cy);//the function may modify numk from its initial value
										   //-------------
										   // Initialize
										   //-------------
	NODE tempnode;
	priority_queue<NODE, vector<NODE>, compare> pq;
	//memset(labels, -1, sz * sizeof(int));
	for (int k = 0; k < numk; k++)
	{
		NODE tempnode;
		tempnode.i = cx[k] << 16 | cy[k];
		tempnode.k = k;
		tempnode.d = 0;
		pq.push(tempnode);
	}
	//vector<double> kY(numk, 0), kU(numk, 0), kV(numk, 0);
	//vector<double> kx(numk, 0), ky(numk, 0);
	for (int k = 0; k < numk; k++) {
		m_apiSuperpixel[k][0] = 0;
		m_apiSuperpixel[k][1] = 0;
		m_apiSuperpixel[k][2] = 0;
		m_apiSuperpixel[k][3] = 0;
		m_apiSuperpixel[k][4] = 0;
	}
	vector<double> ksize(numk, 0);

	const int CONNECTIVITY = 4;//values can be 4 or 8
	const double M = m_dM;//10.0;
	const double invwt = (M*M*numk) / double(sz);

	int qlength = pq.size();
	int pixelcount = 0;
	int ns = m_uiNumOfSuperpix;
	int xx(0), yy(0), ii(0), iiUV(0);
	double Ydiff(0), Udiff(0), Vdiff(0), xdiff(0), ydiff(0), colordist(0), xydist(0), slicdist(0);
	//-------------
	// Run main loop
	//-------------
	while (qlength > 0) //while(nodevec.size() > 0)
	{
		NODE node = pq.top(); pq.pop(); qlength--;
		const int k = node.k;
		const int x = node.i >> 16 & 0xffff;
		const int y = node.i & 0xffff;
		const int i = y * w + x;
		const int iUV = (y >> 1)*(w >> 1) + (x >> 1);

		if (m_cYUVlabel.m_atY[i] == ns)
		{
			m_cYUVlabel.m_atY[i] = k; pixelcount++;
			m_apiSuperpixel[k][0] += YUV.m_atY[i];
			m_apiSuperpixel[k][1] += YUV.m_atU[iUV];
			m_apiSuperpixel[k][2] += YUV.m_atV[iUV];
			m_apiSuperpixel[k][3] += x;
			m_apiSuperpixel[k][4] += y;
			ksize[k] += 1.0;

			for (int p = 0; p < CONNECTIVITY; p++)
			{
				xx = x + dx8[p];
				yy = y + dy8[p];
				if (!(xx < 0 || xx >= w || yy < 0 || yy >= h))
				{
					ii = i + dn8[p];
					iiUV = ((yy) >> 1)*(w >> 1) + ((xx) >> 1);
					if (m_cYUVlabel.m_atY[ii] == ns)//create new nodes
					{
						Ydiff = m_apiSuperpixel[k][0] - YUV.m_atY[ii] * ksize[k];
						Udiff = m_apiSuperpixel[k][1] - YUV.m_atU[iiUV] * ksize[k];
						Vdiff = m_apiSuperpixel[k][2] - YUV.m_atV[iiUV] * ksize[k];
						xdiff = m_apiSuperpixel[k][3] - xx * ksize[k];
						ydiff = m_apiSuperpixel[k][4] - yy * ksize[k];

						colordist = (Ydiff*Ydiff + Udiff * Udiff + Vdiff * Vdiff);

						xydist = xdiff * xdiff + ydiff * ydiff;

						slicdist = (colordist + xydist * invwt) / (ksize[k] * ksize[k]);//late normalization by ksize[k], to have only one division operation

						tempnode.i = xx << 16 | yy;
						tempnode.k = k;
						tempnode.d = slicdist;
						pq.push(tempnode); qlength++;

					}
				}
			}
		}
	}
	m_uiNumOfSuperpix = numk;
	//---------------------------------------------
	// Label the rarely occuring unlabelled pixels
	//---------------------------------------------
	//if (m_cYUVlabel.m_atY[0] == m_uiNumOfSuperpix) m_cYUVlabel.m_atY[0] = 0;
	/*
	for (int y = 1; y < h; y++)
	{
		for (int x = 1; x < w; x++)
		{
			int i = y*w + x;
			if (m_cYUVlabel.m_atY[i] == ns)//find an adjacent label
			{
				if (m_cYUVlabel.m_atY[i - 1] < ns) m_cYUVlabel.m_atY[i] = m_cYUVlabel.m_atY[i - 1];
				else if (m_cYUVlabel.m_atY[i - w] < ns) m_cYUVlabel.m_atY[i] = m_cYUVlabel.m_atY[i - w];
			}//if labels[i] < 0 ends
		}
	}//*/

	for (int k = 0; k < numk; k++) {
		m_apiSuperpixel[k][0] /= ksize[k];
		m_apiSuperpixel[k][1] /= ksize[k];
		m_apiSuperpixel[k][2] /= ksize[k];
		m_apiSuperpixel[k][3] /= ksize[k];
		m_apiSuperpixel[k][4] /= ksize[k];

		if (m_apiSuperpixel[k][3] >= m_uiSourceWidth || m_apiSuperpixel[k][4] >= m_uiSourceHeight) {
			m_apiSuperpixel[k][3] = 0;
			m_apiSuperpixel[k][4] = 0;
			std::cout << '1';
		}
	}

}


//===========================================================================
/// runSNIC
///
/// Runs the priority queue base Simple Non-Iterative Clustering (SNIC) algo.
//===========================================================================
Void cSNICSuperpixel::runSNIC(
	ImagePixelType*		Yv,
	ImagePixelType*		Uv,
	ImagePixelType*		Vv,
	const int					width,
	const int					height,
	int*					   labels,
	int*						outnumk,
	const int				  innumk,
	const double			   compactness)
{
	const int w = width;
	const int h = height;
	const int sz = w * h;
	const int dx8[8] = { -1,  0, 1, 0, -1,  1, 1, -1 };//for 4 or 8 connectivity
	const int dy8[8] = { 0, -1, 0, 1, -1, -1, 1,  1 };//for 4 or 8 connectivity
	const int dn8[8] = { -1, -w, 1, w, -1 - w,1 - w,1 + w,-1 + w };
	const int dn8UV[8] = { -1, -(width >> 1), 1, (width >> 1), -1 - (width >> 1),1 - (width >> 1),1 + (width >> 1),-1 + (width >> 1) };


	struct NODE
	{
		unsigned int i; // the x and y values packed into one
		unsigned int k; // the label
		double d;	   // the distance
	};
	struct compare
	{
		bool operator()(const NODE& one, const NODE& two)
		{
			return one.d > two.d;//for increasing order of distances
		}
	};
	//-------------
	// Find seeds
	//-------------
	cArray<int> cx, cy;
	int numk = innumk;
	FindSeeds(width, height, numk, cx, cy);//the function may modify numk from its initial value
	//-------------
	// Initialize
	//-------------
	NODE tempnode;
	priority_queue<NODE, vector<NODE>, compare> pq;
	memset(labels, -1, sz * sizeof(int));
	for (int k = 0; k < numk; k++)
	{
		NODE tempnode;
		tempnode.i = cx[k] << 16 | cy[k];
		tempnode.k = k;
		tempnode.d = 0;
		pq.push(tempnode);
	}
	vector<double> kY(numk, 0), kU(numk, 0), kV(numk, 0);
	vector<double> kx(numk, 0), ky(numk, 0);
	vector<double> ksize(numk, 0);

	const int CONNECTIVITY = 8;//values can be 4 or 8
	const double M = compactness;//10.0;
	const double invwt = (M*M*numk) / double(sz);

	int qlength = pq.size();
	int pixelcount = 0;
	int xx(0), yy(0), ii(0), iiUV(0);
	double Ydiff(0), Udiff(0), Vdiff(0), xdiff(0), ydiff(0), colordist(0), xydist(0), slicdist(0);
	//-------------
	// Run main loop
	//-------------
	while (qlength > 0) //while(nodevec.size() > 0)
	{
		NODE node = pq.top(); pq.pop(); qlength--;
		const int k = node.k;
		const int x = node.i >> 16 & 0xffff;
		const int y = node.i & 0xffff;
		const int i = y * width + x;
		const int iUV = (y >> 1)*(width >> 1) + (x >> 1);

		if (labels[i] < 0)
		{
			labels[i] = k; pixelcount++;
			kY[k] += Yv[i];
			kU[k] += Uv[iUV];
			kV[k] += Vv[iUV];
			kx[k] += x;
			ky[k] += y;
			ksize[k] += 1.0;

			for (int p = 0; p < CONNECTIVITY; p++)
			{
				xx = x + dx8[p];
				yy = y + dy8[p];
				if (!(xx < 0 || xx >= w || yy < 0 || yy >= h))
				{
					ii = i + dn8[p];
					iiUV = iUV;
					if (labels[ii] < 0)//create new nodes
					{
						Ydiff = kY[k] - Yv[ii] * ksize[k];
						Udiff = kU[k] - Uv[iiUV] * ksize[k];
						Vdiff = kV[k] - Vv[iiUV] * ksize[k];
						xdiff = kx[k] - xx * ksize[k];
						ydiff = ky[k] - yy * ksize[k];

						colordist = Ydiff * Ydiff + Udiff * Udiff + Vdiff * Vdiff;
						xydist = xdiff * xdiff + ydiff * ydiff;
						slicdist = (colordist + xydist * invwt) / (ksize[k] * ksize[k]);//late normalization by ksize[k], to have only one division operation

						tempnode.i = xx << 16 | yy;
						tempnode.k = k;
						tempnode.d = slicdist;
						pq.push(tempnode); qlength++;

					}
				}
			}
		}
	}
	*outnumk = numk;
	//---------------------------------------------
	// Label the rarely occuring unlabelled pixels
	//---------------------------------------------
	if (labels[0] < 0) labels[0] = 0;
	for (int y = 1; y < height; y++)
	{
		for (int x = 1; x < width; x++)
		{
			int i = y * width + x;
			if (labels[i] < 0)//find an adjacent label
			{
				if (labels[i - 1] >= 0) labels[i] = labels[i - 1];
				else if (labels[i - width] >= 0) labels[i] = labels[i - width];
			}//if labels[i] < 0 ends
		}
	}

}
