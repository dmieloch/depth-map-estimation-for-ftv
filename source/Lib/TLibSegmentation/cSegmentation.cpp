//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cSegmentation.h"
#include <cmath>
#include <math.h>

using namespace std;

cSegmentation::cSegmentation() {
	return;
}

Void cSegmentation::init(UInt SourceWidth, UInt SourceHeight, Double M, UInt K) {
	m_uiSourceWidth = SourceWidth;
	m_uiSourceHeight = SourceHeight;
	m_dM = M;
	m_uiK = K;

	m_uiSuperpixelSize = UInt(sqrt(m_uiSourceHeight*m_uiSourceWidth / Double(m_uiK)));

	m_cYUVlabel.init(m_uiSourceWidth, m_uiSourceHeight, 32, 400);
	m_cYUVdist.init(m_uiSourceWidth, m_uiSourceHeight, 64, 400);

	m_uiNumOfSuperpix = 0;
	for (UInt h = m_uiSuperpixelSize / 2; h < m_uiSourceHeight; h += m_uiSuperpixelSize) {
		for (UInt w = m_uiSuperpixelSize / 2; w < m_uiSourceWidth; w += m_uiSuperpixelSize) {
			m_uiNumOfSuperpix++;
		}
	}

	for (UInt h = 0, pp = 0; h < m_uiSourceHeight; h++) {
		for (UInt w = 0; w < m_uiSourceWidth; w++, pp++) {
			m_cYUVlabel.m_atY[pp] = m_uiNumOfSuperpix;
			m_cYUVdist.m_atY[pp] = DBL_MAX;
		}
	}



	for (UInt i = 0; i < m_uiNumOfSuperpix; i++) {
		cArray<Double> Superpix;
		for (UInt s = 0; s < 6; s++) {
			Double temp = 0;
			Superpix.push_back(temp);
		}
		m_apiSuperpixel.push_back(Superpix);
	}

	return;
}


Void cSegmentation::compute(cYUV<ImagePixelType> &YUV, UInt uiFrameId, UInt* &cYUVlabel, UInt uiTemporalEnhancementMethod) {
	return;
}

Void cSegmentation::compute_neigh() {
	for (Int s = 0; s < m_uiNumOfSuperpix; s++) {
		cArray<UInt> nei;
		nei.reserve(50);
		Superpixel_neigh.push_back(nei);
	}

	for (Int h = 0; h < m_uiSourceHeight; h++) {
		for (Int w = 0; w < m_uiSourceWidth; w++) {
			UInt cur_label = m_cYUVlabel.m_atY[h*m_uiSourceWidth + w];
			if (h - 1 > 0 && w - 1 > 0 && h + 2 < m_uiSourceHeight && w + 2 < m_uiSourceWidth) {
				for (Int y = h - 1; y < h + 1; y++) {
					for (Int x = w; x < w + 2; x++) {
						if (cur_label != m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) {
							Bool present = false;

							for (UInt s = 0; s < Superpixel_neigh.at(cur_label).size(); s++) {
								if (Superpixel_neigh.at(cur_label).at(s) == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) {
									present = true;
									break;
								}
							}
							if ((!present)) {
								Superpixel_neigh.at(cur_label).push_back(m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]);
							}
						}
					}
				}
			}
		}
	}
	//std::cout << Superpixel_neigh.at(106470).size() << ' ';
}

Void cSegmentation::compute_neigh_refine() {
	for (Int s = 0; s < m_uiNumOfSuperpix; s++) {
		cArray<UInt> nei;
		//nei.reserve(50);
		Superpixel_neigh_refine.push_back(nei);
	}

	for (Int h = 2; h < m_uiSourceHeight - 2; h++) {
		for (Int w = 2; w < m_uiSourceWidth - 2; w++) {
			UInt cur_label = m_cYUVlabel.m_atY[h*m_uiSourceWidth + w];
			if (h - 2 > 0 && w - 2 > 0 && h + 3 < m_uiSourceHeight && w + 3 < m_uiSourceWidth) {
				for (Int y = h - 1; y < h + 2; y++) {
					for (Int x = w - 1; x < w + 2; x++) {
						if (cur_label != m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) {
							Bool present = false;
							for (UInt s = 0; s < Superpixel_neigh_refine.at(cur_label).size(); s++) {
								if (Superpixel_neigh_refine.at(cur_label).at(s) == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) {
									present = true;
									break;
								}
							}
							if ((!present)) {
								Superpixel_neigh_refine.at(cur_label).push_back(m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]);
							}
						}
					}
				}
			}
		}
	}
}

Void cSegmentation::qSort(UInt* tab, Int left, Int right) {
	Int i = left;
	Int j = right;
	Int x = tab[(left + right) / 2];
	do {
		while (tab[i] < x) i++;
		while (tab[j] > x) j--;
		if (i <= j)	std::swap(tab[i++], tab[j--]);
	} while (i <= j);

	if (left < j) qSort(tab, left, j);
	if (right > i) qSort(tab, i, right);
}


Void cSegmentation::refine_depth(UInt* rapuiDepthLabel, UInt luma_thr, UInt depth_thr) {

	compute_neigh_refine();

	UInt* puiDepthLabelTemp = new UInt[m_uiNumOfSuperpix];
	for (UInt uiSPp = 0; uiSPp < m_uiNumOfSuperpix; uiSPp++) {
		puiDepthLabelTemp[uiSPp] = rapuiDepthLabel[uiSPp];
	}

	for (UInt uiSPp = 0; uiSPp < m_uiNumOfSuperpix; uiSPp++) {
		Int cnt = 0;
		for (Int uiSPnc = 0; uiSPnc < Superpixel_neigh_refine.at(uiSPp).size(); uiSPnc++) {
			UInt uiSPn = Superpixel_neigh_refine.at(uiSPp).at(uiSPnc);
			if ((Abs(m_apiSuperpixel[uiSPp][0] - m_apiSuperpixel[uiSPn][0]) < luma_thr) &&
				(Abs(m_apiSuperpixel[uiSPp][1] - m_apiSuperpixel[uiSPn][1]) < luma_thr) &&
				(Abs(m_apiSuperpixel[uiSPp][2] - m_apiSuperpixel[uiSPn][2]) < luma_thr)) {
				cnt++;
			}
		}

		UInt *temp_dm = new UInt[cnt];

		cnt = 0;
		for (Int uiSPnc = 0; uiSPnc < Superpixel_neigh_refine.at(uiSPp).size(); uiSPnc++) {
			UInt uiSPn = Superpixel_neigh_refine.at(uiSPp).at(uiSPnc);
			if ((Abs(m_apiSuperpixel[uiSPp][0] - m_apiSuperpixel[uiSPn][0]) < luma_thr) &&
				(Abs(m_apiSuperpixel[uiSPp][1] - m_apiSuperpixel[uiSPn][1]) < luma_thr) &&
				(Abs(m_apiSuperpixel[uiSPp][2] - m_apiSuperpixel[uiSPn][2]) < luma_thr)) {
				temp_dm[cnt++] = rapuiDepthLabel[uiSPn];
				//cout << rapuiDepthLabel[uiSPn] << ' ';
			}
		}



		if (cnt > 1)qSort(temp_dm, 0, cnt - 1);

		if ((Abs((Int)rapuiDepthLabel[uiSPp] - (Int)temp_dm[cnt / 2]) > depth_thr) && (cnt > 2)) {
			puiDepthLabelTemp[uiSPp] = temp_dm[cnt / 2];

		}
		delete temp_dm;

	}

	for (UInt uiSPp = 0; uiSPp < m_uiNumOfSuperpix; uiSPp++) {
		rapuiDepthLabel[uiSPp] = puiDepthLabelTemp[uiSPp];
	}

	delete puiDepthLabelTemp;

	return;
}

/*Void cSuperpixel::refine_depth(cYUV<DepthPixelType> &YUVdm, UInt uiInterp, String filename, Bool append, UInt luma_thr, UInt depth_thr) {

	UInt range = 3;




	for (UInt s = 0; s<m_uiNumOfSuperpix; s++) {
		Int temp_x = m_apiSuperpixel[s][3];
		Int temp_y = m_apiSuperpixel[s][4];

		Int temp = 0;
		UInt cnt = 0;
		for (Int y = temp_y - m_uiSuperpixelSize*range; y<temp_y + m_uiSuperpixelSize*range; y++) {
			for (Int x = temp_x - m_uiSuperpixelSize*range; x<temp_x + m_uiSuperpixelSize*range; x++) {
				if (y>0 && y<m_uiSourceHeight && x>0 && x<m_uiSourceWidth) {
					if (s == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) cnt++;
					for (UInt v = 0; v<Superpixel_neigh.at(s).size(); v++) {
						if ((Superpixel_neigh[s][v] == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x])&&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][0] - m_apiSuperpixel[s][0])<luma_thr) &&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][1] - m_apiSuperpixel[s][1])<luma_thr / 2) &&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][2] - m_apiSuperpixel[s][2])<luma_thr / 2)) {
							cnt++;
						}
					}
				}
			}
		}

		DepthPixelType *temp_dm = new DepthPixelType[cnt];
		cnt = 0;
		for (Int y = temp_y - m_uiSuperpixelSize*range; y<temp_y + m_uiSuperpixelSize*range; y++) {
			for (Int x = temp_x - m_uiSuperpixelSize*range; x<temp_x + m_uiSuperpixelSize*range; x++) {
				if (y>0 && y<m_uiSourceHeight && x>0 && x<m_uiSourceWidth) {
					if ((s == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x])) temp_dm[cnt++] = YUVdm.m_atY[y*m_uiSourceWidth + x];
					for (UInt v = 0; v<Superpixel_neigh.at(s).size(); v++) {
						if ((Superpixel_neigh[s][v] == m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]) &&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][0] - m_apiSuperpixel[s][0])<luma_thr) &&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][1] - m_apiSuperpixel[s][1])<luma_thr / 2) &&
							(Abs(m_apiSuperpixel[m_cYUVlabel.m_atY[y*m_uiSourceWidth + x]][2] - m_apiSuperpixel[s][2])<luma_thr / 2)) {
							temp_dm[cnt++] = YUVdm.m_atY[y*m_uiSourceWidth + x];
						}
					}
				}
			}
		}
		if (cnt>1)qSort(temp_dm, 0, cnt - 1);
		//Int tempd = 0;
		//for (Int i=0; i<cnt; i++){
		//	tempd+=temp_dm[i];
		//}
		//tempd/=(Double)cnt;

		for (Int y = temp_y - m_uiSuperpixelSize*range; y<temp_y + m_uiSuperpixelSize*range; y++) {
			for (Int x = temp_x - m_uiSuperpixelSize*range; x<temp_x + m_uiSuperpixelSize*range; x++) {
				if (y>0 && y<m_uiSourceHeight && x>0 && x<m_uiSourceWidth) {
					if (m_cYUVlabel.m_atY[y*m_uiSourceWidth + x] == s) {
						if (Abs(YUVdm.m_atY[y*m_uiSourceWidth + x] - temp_dm[cnt / 2])>depth_thr && cnt>0) YUVdm.m_atY[y*m_uiSourceWidth + x] = temp_dm[cnt / 2 - 1];
						//if(Abs(YUVdm.m_atY[y*m_uiSourceWidth+x]-tempd)>depth_thr && cnt>0) YUVdm.m_atY[y*m_uiSourceWidth+x]=(DepthPixelType)tempd;
					}
				}
			}
		}
		delete temp_dm;
	}

	YUVdm.frameWriter(filename, append);

	return;
}*/

Void cSegmentation::write_segmented_depth(cYUV<DepthPixelType> &YUVdm, UInt uiDepthWidth, UInt uiDepthHeight, String filename, Bool append) {

	cYUV<DepthPixelType> pcYUVdepth_out = cYUV<DepthPixelType>(m_uiSourceWidth, m_uiSourceHeight, DEPTH_BPS, 400);
	UInt uiInterp = _logb(m_uiSourceHeight / uiDepthHeight);
	for (UInt h = 0; h < m_uiSourceHeight; h++) {
		for (UInt w = 0; w < m_uiSourceWidth; w++) {
			if (m_cYUVlabel.m_atY[h*m_uiSourceWidth + w] < m_uiNumOfSuperpix) {
				if (h - 1 > 0 && w - 1 > 0 && h + 1 < uiDepthHeight && w + 1 < uiDepthWidth) {
					if (m_cYUVlabel.m_atY[(h - 1)*m_uiSourceWidth + w - 1] == m_cYUVlabel.m_atY[(h - 1)*m_uiSourceWidth + w] &&
						m_cYUVlabel.m_atY[(h - 1)*m_uiSourceWidth + w] == m_cYUVlabel.m_atY[h*m_uiSourceWidth + w + 1] &&
						m_cYUVlabel.m_atY[(h - 1)*m_uiSourceWidth + w + 1] == m_cYUVlabel.m_atY[h*m_uiSourceWidth + w - 1] &&
						m_cYUVlabel.m_atY[h*m_uiSourceWidth + w - 1] == m_cYUVlabel.m_atY[h*m_uiSourceWidth + w + 1] &&
						m_cYUVlabel.m_atY[h*m_uiSourceWidth + w + 1] == m_cYUVlabel.m_atY[(h + 1)*m_uiSourceWidth + w - 1] &&
						m_cYUVlabel.m_atY[(h + 1)*m_uiSourceWidth + w - 1] == m_cYUVlabel.m_atY[(h + 1)*m_uiSourceWidth + w + 1] &&
						m_cYUVlabel.m_atY[(h + 1)*m_uiSourceWidth + w - 1] != m_cYUVlabel.m_atY[(h)*m_uiSourceWidth + w]) {
						m_cYUVlabel.m_atY[(h)*m_uiSourceWidth + w] = m_cYUVlabel.m_atY[(h + 1)*m_uiSourceWidth + w - 1];
					}

				}

				UInt x = m_apiSuperpixel[m_cYUVlabel.m_atY[h*m_uiSourceWidth + w]][3];
				UInt y = m_apiSuperpixel[m_cYUVlabel.m_atY[h*m_uiSourceWidth + w]][4];
				pcYUVdepth_out.m_atY[h*m_uiSourceWidth + w] = YUVdm.m_atY[(y >> uiInterp)*(m_uiSourceWidth >> uiInterp) + (x >> uiInterp)];
			}
		}
	}
	pcYUVdepth_out.frameWriter(filename, append);
	return;
}

Void cSegmentation::write_segmented_tex(cYUV<ImagePixelType> *YUV) {

	//cYUV<ImagePixelType> *pcYUVout = new cYUV<ImagePixelType>(m_uiSourceWidth, m_uiSourceHeight, VIDEO_BPS, 420);
	for (UInt h = 0; h < m_uiSourceHeight; h++) {
		for (UInt w = 0; w < m_uiSourceWidth; w++) {
			if (m_cYUVlabel.m_atY[h*m_uiSourceWidth + w] < m_uiNumOfSuperpix) {
				YUV->m_atY[h*m_uiSourceWidth + w] = m_apiSuperpixel[m_cYUVlabel.m_atY[h*m_uiSourceWidth + w]][0];
				YUV->m_atU[(h >> 1)*(m_uiSourceWidth >> 1) + (w >> 1)] = m_apiSuperpixel[m_cYUVlabel.m_atY[h*m_uiSourceWidth + w]][1];
				YUV->m_atV[(h >> 1)*(m_uiSourceWidth >> 1) + (w >> 1)] = m_apiSuperpixel[m_cYUVlabel.m_atY[h*m_uiSourceWidth + w]][2];
			}
		}
	}
	//pcYUVout->frameWriter(filename,append);
	return;
}

Void cSegmentation::write_labels(String filename, Bool append) {

	cYUV<UShort> cYUVwritelabel;
	cYUVwritelabel.init(m_uiSourceWidth, m_uiSourceHeight, 16, 400);
	UInt uiNorm = 65535 / m_uiNumOfSuperpix;
	for (UInt y = 0; y < m_uiSourceHeight; y++) {
		for (UInt x = 0; x < m_uiSourceWidth; x++) {
			cYUVwritelabel.m_atY[y*m_uiSourceWidth + x] = m_cYUVlabel.m_atY[y*m_uiSourceWidth + x] * uiNorm;
		}
	}
	cYUVwritelabel.frameWriter(filename, append);
	return;
}
