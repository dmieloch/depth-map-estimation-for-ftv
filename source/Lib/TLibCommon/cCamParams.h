//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __CCAMPARAMS_H__
#define __CCAMPARAMS_H__

#include "TypeDef.h"
#include "CommonDef.h"
#include "cMatrix.h"
#include "cArray.h"
#include <fstream>

template <class MatrixType>
class cCamParams {
public:
	Char m_sCamName[MAX_TEXT_LENGTH];

	cMatrix<MatrixType> m_mExtMat; //4x4
	cMatrix<MatrixType> m_mIntMat; //4x4

	MatrixType m_tZnear;
	MatrixType m_tZfar;

	cMatrix<MatrixType> m_mCvtdExtMat; //4x4
	cMatrix<MatrixType> m_mProjMat; //4x4
	cMatrix<MatrixType> m_mInvProjMat; //4x4

	cCamParams();

	Void cvtExtMat();
	Void calcProjMat();
	Void calcInvProjMat();
};

template <class MatrixType>
cCamParams<MatrixType>::cCamParams() {
	return;
}

template <class MatrixType>
Void cCamParams<MatrixType>::cvtExtMat() {

	cMatrix<MatrixType> R(3, 3);
	cMatrix<MatrixType> T(3, 1);

	m_mCvtdExtMat.init(4, 4);

	for (UInt h = 0; h < 3; h++) {
		for (UInt w = 0; w < 3; w++) {
			m_mCvtdExtMat.at(h, w) = m_mExtMat.at(h, w);
			R.at(h, w) = m_mExtMat.at(h, w);
		}
		T.at(h, 0) = m_mExtMat.at(h, 3);
	}

	cMatrix<MatrixType> mRT;
	mRT.init(3, 1);
	MatrixMultiply(R, T, mRT);

	for (UInt w = 0; w < 3; w++) m_mCvtdExtMat.at(3, w) = 0;
	for (UInt h = 0; h < 3; h++) m_mCvtdExtMat.at(h, 3) = -mRT.at(h, 0);
	m_mCvtdExtMat.at(3, 3) = 1;

	return;
}

template <class MatrixType>
Void cCamParams<MatrixType>::calcProjMat() {

	m_mProjMat.init(4, 4);
	MatrixMultiply(m_mIntMat, m_mCvtdExtMat, m_mProjMat);

	return;
}

template <class MatrixType>
Void cCamParams<MatrixType>::calcInvProjMat() {

	m_mInvProjMat.init(4, 4);
	invert(m_mProjMat, m_mInvProjMat);

	return;
}

template <class MatrixType>
Void readParamsFromFile(String sFilename, cArray<cCamParams<MatrixType>*> &rapcCamParams, cArray<String> cfgCamnames) {

	cCamParams<MatrixType>* pcCamParams = NULL;

	Char tmp[MAX_TEXT_LENGTH];

	for (UInt i = 0; i < cfgCamnames.size(); i++) {
		pcCamParams = new cCamParams<MatrixType>(); rapcCamParams.push_back(pcCamParams);
	}

	pcCamParams = NULL;

	FILE *fParamFile;
	fopen_s(&fParamFile, sFilename.c_str(), "r");

	if (!fParamFile) {
		fprintf(stdout, "Camera parameters file %s doesn't exist\n", sFilename.c_str());
	}

	while (!feof(fParamFile)) {
		fscanf_s(fParamFile, "%s", tmp, MAX_TEXT_LENGTH);
		for (UInt uiCamId = 0; uiCamId < cfgCamnames.size(); uiCamId++) {
			if (!strcmp(tmp, cfgCamnames[uiCamId].c_str())) {
				//Found neccesarry name
				pcCamParams = rapcCamParams[uiCamId];
				break;
			}
		}
		if (pcCamParams) {
			strcpy_s(pcCamParams->m_sCamName, MAX_TEXT_LENGTH, tmp);
			if (feof(fParamFile)) break;

			pcCamParams->m_mExtMat.init(4, 4);
			pcCamParams->m_mIntMat.init(4, 4);

			for (Int h = 0; h < 3; h++) pcCamParams->m_mIntMat.at(h, 3) = 0;
			for (Int w = 0; w < 3; w++) pcCamParams->m_mIntMat.at(3, w) = 0;
			pcCamParams->m_mIntMat.at(3, 3) = 1;

			for (Int h = 0; h < 3; h++) {
				for (Int w = 0; w < 3; w++) {
					fscanf_s(fParamFile, "%s", tmp, MAX_TEXT_LENGTH);
					pcCamParams->m_mIntMat.at(h, w) = atof(tmp);
				}
			}

			fscanf_s(fParamFile, "%s", tmp, MAX_TEXT_LENGTH);
			fscanf_s(fParamFile, "%s", tmp, MAX_TEXT_LENGTH);

			for (Int w = 0; w < 3; w++) pcCamParams->m_mExtMat.at(3, w) = 0;
			pcCamParams->m_mExtMat.at(3, 3) = 1;

			for (Int h = 0; h < 3; h++) {
				for (Int w = 0; w < 4; w++) {
					fscanf_s(fParamFile, "%s", tmp, MAX_TEXT_LENGTH);
					pcCamParams->m_mExtMat.at(h, w) = atof(tmp);
				}
			}
			pcCamParams->cvtExtMat();
			pcCamParams->calcProjMat();
			pcCamParams->calcInvProjMat();
		}
		pcCamParams = NULL;
	}

	fclose(fParamFile);
}

#endif