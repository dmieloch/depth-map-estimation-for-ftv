//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __CYUV_H__
#define __CYUV_H__

#include "TypeDef.h"
#include "CommonDef.h"
#include <iostream>
#include <fstream>

template <class PixelType>
class cYUV {
public:
	Char m_sYUVFilename[MAX_TEXT_LENGTH];

	UInt m_uiWidth;
	UInt m_uiHeight;
	UInt m_uiWidthUV;
	UInt m_uiHeightUV;

	UInt m_uiOriginalWidth;
	UInt m_uiOriginalHeight;

	UInt m_uiImSize;
	UInt m_uiImSizeUV;

	UInt m_uiBitsPerSample;
	UInt m_uiChromaSubsampling;

	Int m_iFilterType;
	Double m_uiHorizontalPrecision;
	Double m_uiVerticalPrecision;

	PixelType *m_atY;
	PixelType *m_atU;
	PixelType *m_atV;

	~cYUV();
	cYUV();
	cYUV(Int w, Int h, Int bps = 8, Int cs = 420);
	Void setOriginalSize();
	Void init(Int w, Int h, Int bps = 8, Int cs = 420);
	Void frameReader(String sFileName, Int iFrame);
	Void frameWriter(String sFileName, Bool bAppend);

	Void imResize(Int filterType, Double hPrec, Double vPrec);
	Void imResizeHorizontalY();
	Void imResizeVerticalY();
	Void imResizeHorizontalUV();
	Void imResizeVerticalUV();

	UInt getChrominanceHorizontalShift();
	UInt getChrominanceVerticalShift();
};

template <class PixelType>
cYUV<PixelType>::~cYUV() {
	if (m_atY) { delete[] m_atY; m_atY = NULL; }
	if (m_atU) { delete[] m_atU; m_atU = NULL; }
	if (m_atV) { delete[] m_atV; m_atV = NULL; }
}

template <class PixelType>
cYUV<PixelType>::cYUV() {
	m_atY = NULL;
	m_atU = NULL;
	m_atV = NULL;
	return;
}

template <class PixelType>
cYUV<PixelType>::cYUV(Int w, Int h, Int bps = 8, Int cs = 420) {
	m_atY = NULL;
	m_atU = NULL;
	m_atV = NULL;
	init(w, h, bps, cs);
	return;
}

template <class PixelType>
Void cYUV<PixelType>::setOriginalSize() {

	if (m_atY) { delete m_atY; m_atY = NULL; }
	if (m_atU) { delete m_atU; m_atU = NULL; }
	if (m_atV) { delete m_atV; m_atV = NULL; }

	m_uiWidth = m_uiOriginalWidth;
	m_uiHeight = m_uiOriginalHeight;
	m_uiImSize = m_uiWidth * m_uiHeight;

	m_atY = new PixelType[m_uiImSize];

	if (m_uiChromaSubsampling == 400) {
		m_uiWidthUV = 0;
		m_uiHeightUV = 0;
		m_uiImSizeUV = 0;
		m_atU = NULL;
		m_atV = NULL;
	}
	if (m_uiChromaSubsampling == 420) {
		m_uiWidthUV = m_uiWidth >> 1;
		m_uiHeightUV = m_uiHeight >> 1;
		m_uiImSizeUV = m_uiWidthUV * m_uiHeightUV;
		m_atU = new PixelType[m_uiImSizeUV];
		m_atV = new PixelType[m_uiImSizeUV];
	}
	if (m_uiChromaSubsampling == 444) {
		m_uiWidthUV = m_uiWidth;
		m_uiHeightUV = m_uiHeight;
		m_uiImSizeUV = m_uiWidthUV * m_uiHeightUV;
		m_atU = new PixelType[m_uiImSizeUV];
		m_atV = new PixelType[m_uiImSizeUV];
	}

}

template <class PixelType>
Void cYUV<PixelType>::init(Int w, Int h, Int bps = 8, Int cs = 420) {

	m_uiOriginalHeight = h;
	m_uiOriginalWidth = w;

	m_uiBitsPerSample = bps;
	m_uiChromaSubsampling = cs;

	m_uiWidth = m_uiOriginalWidth;
	m_uiHeight = m_uiOriginalHeight;
	m_uiImSize = m_uiWidth * m_uiHeight*(m_uiBitsPerSample >> 3);

	setOriginalSize();
	return;
}


template <class PixelType>
Void cYUV<PixelType>::frameReader(String sFileName, Int frame) {

	setOriginalSize();

	FILE* fileYUV = NULL;
	fopen_s(&fileYUV, sFileName.c_str(), "rb");

	Int64 offset;
	if (m_uiChromaSubsampling == 444) offset = 3 * m_uiImSize * m_uiBitsPerSample >> 3;;
	if (m_uiChromaSubsampling == 420) offset = Int(m_uiImSize + (m_uiImSize >> 1)) * m_uiBitsPerSample >> 3;
	if (m_uiChromaSubsampling == 400) offset = m_uiImSize * m_uiBitsPerSample >> 3;

	_fseeki64(fileYUV, (Int64)offset*(Int64)frame, 0);

	fread(m_atY, m_uiBitsPerSample >> 3, m_uiImSize, fileYUV);

	if (m_uiChromaSubsampling == 420 || m_uiChromaSubsampling == 444) {
		fread(m_atU, m_uiBitsPerSample >> 3, m_uiImSizeUV, fileYUV);
		fread(m_atV, m_uiBitsPerSample >> 3, m_uiImSizeUV, fileYUV);
	}

	fclose(fileYUV);

	return;
}

template <class PixelType>
Void cYUV<PixelType>::frameWriter(String sFileName, Bool bAppend) {

	FILE* fileYUV;

	fopen_s(&fileYUV, sFileName.c_str(), (bAppend) ? "ab" : "wb");

	fwrite((void*)m_atY, m_uiBitsPerSample >> 3, m_uiImSize, fileYUV);

	if (m_atU && m_atV) {
		fwrite(m_atU, m_uiBitsPerSample >> 3, m_uiImSizeUV, fileYUV);
		fwrite(m_atV, m_uiBitsPerSample >> 3, m_uiImSizeUV, fileYUV);
	}

	fclose(fileYUV);

	return;
}

template <class PixelType>
Void cYUV<PixelType>::imResizeHorizontalY() {

	Int leftPix, rightPix;
	Int* Pix = new Int[(Int)m_uiHorizontalPrecision];
	UInt newWidth = UInt(m_uiWidth*m_uiHorizontalPrecision);
	UInt pWidth_minus1 = m_uiWidth - 1;

	cYUV<PixelType> *temp = new cYUV<PixelType>(newWidth, m_uiHeight);

	switch ((Int)m_uiHorizontalPrecision) {
	case 2:
		for (UInt i = 0; i < this->m_uiWidth; i++) {
			leftPix = Clip3(i, 0, pWidth_minus1);
			rightPix = Clip3(i + 1, 0, pWidth_minus1);
			*Pix = i << 1;
			*(Pix + 1) = *Pix + 1;

			for (UInt j = 0; j < this->m_uiHeight; j++) {
				temp->m_atY[j*newWidth + *Pix] = this->m_atY[j*m_uiWidth + leftPix];
				temp->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atY[j*m_uiWidth + leftPix] + this->m_atY[j*m_uiWidth + rightPix] + 1) >> 1;
			}
		}
		break;

	case 4:
		for (UInt i = 0; i < this->m_uiWidth; i++) {
			leftPix = Clip3(i, 0, pWidth_minus1);
			rightPix = Clip3(i + 1, 0, pWidth_minus1);
			*Pix = i << 2;
			*(Pix + 1) = *Pix + 1;
			*(Pix + 2) = *(Pix + 1) + 1;
			*(Pix + 3) = *(Pix + 2) + 1;

			for (UInt j = 0; j < this->m_uiHeight; j++) {
				temp->m_atY[j*newWidth + *Pix] = this->m_atY[j*m_uiWidth + leftPix];
				temp->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atY[j*m_uiWidth + leftPix] * 3 + this->m_atY[j*m_uiWidth + rightPix] + 2) >> 2;
				temp->m_atY[j*newWidth + *(Pix + 2)] = (this->m_atY[j*m_uiWidth + leftPix] + this->m_atY[j*m_uiWidth + rightPix] + 1) >> 1;
				temp->m_atY[j*newWidth + *(Pix + 3)] = (this->m_atY[j*m_uiWidth + leftPix] + this->m_atY[j*m_uiWidth + rightPix] * 3 + 2) >> 2;
			}
		}
		break;
	}



	delete this->m_atY;
	this->m_atY = temp->m_atY;
	this->m_uiWidth = newWidth;
	this->m_uiImSize = newWidth * this->m_uiHeight*(m_uiBitsPerSample >> 3);
}

template <class PixelType>
Void cYUV<PixelType>::imResizeVerticalY() {

	Int topPix, bottomPix;
	Int* Pix = new Int[(Int)m_uiVerticalPrecision];
	UInt newHeight = UInt(this->m_uiHeight*m_uiVerticalPrecision);
	UInt height_minus1 = this->m_uiHeight - 1;

	cYUV<PixelType> *temp = new cYUV<PixelType>(this->m_uiWidth, newHeight);

	switch ((Int)m_uiVerticalPrecision) {
	case 2:
		for (UInt j = 0; j < this->m_uiHeight; j++) {
			topPix = Clip3(j, 0, height_minus1);
			bottomPix = Clip3(j + 1, 0, height_minus1);
			*Pix = j << 1;
			*(Pix + 1) = *Pix + 1;
			for (UInt i = 0; i < this->m_uiWidth; i++) {
				temp->m_atY[*Pix * this->m_uiWidth + i] = this->m_atY[topPix * this->m_uiWidth + i];
				temp->m_atY[*(Pix + 1) * this->m_uiWidth + i] = (this->m_atY[topPix * this->m_uiWidth + i] + this->m_atY[bottomPix * this->m_uiWidth + i] + 1) >> 1;
			}
		}
		break;

	case 4:
		for (UInt j = 0; j < this->m_uiHeight; j++) {
			topPix = Clip3(j, 0, height_minus1);
			bottomPix = Clip3(j + 1, 0, height_minus1);
			*Pix = j << 2;
			*(Pix + 1) = *Pix + 1;
			*(Pix + 2) = *(Pix + 1) + 1;
			*(Pix + 3) = *(Pix + 2) + 1;
			for (UInt i = 0; i < this->m_uiWidth; i++) {
				temp->m_atY[*Pix * this->m_uiWidth + i] = this->m_atY[topPix * this->m_uiWidth + i];
				temp->m_atY[*(Pix + 1) * this->m_uiWidth + i] = (this->m_atY[topPix * this->m_uiWidth + i] * 3 + this->m_atY[bottomPix * this->m_uiWidth + i] + 2) >> 2;
				temp->m_atY[*(Pix + 2) * this->m_uiWidth + i] = (this->m_atY[topPix * this->m_uiWidth + i] + this->m_atY[bottomPix * this->m_uiWidth + i] + 1) >> 1;
				temp->m_atY[*(Pix + 3) * this->m_uiWidth + i] = (this->m_atY[topPix * this->m_uiWidth + i] + this->m_atY[bottomPix * this->m_uiWidth + i] * 3 + 2) >> 2;
			}
		}
		break;
	}
	delete this->m_atY;
	this->m_atY = temp->m_atY;
	this->m_uiHeight = newHeight;
	this->m_uiImSize = this->m_uiWidth*newHeight*(m_uiBitsPerSample >> 3);
}

template <class PixelType>
Void cYUV<PixelType>::imResizeHorizontalUV() {

	Int leftPix, rightPix;
	Int* Pix = new Int[(Int)m_uiHorizontalPrecision];
	UInt newWidth = UInt(m_uiWidthUV*m_uiHorizontalPrecision);
	UInt width_minus1 = this->m_uiWidthUV - 1;
	UInt height = this->m_uiHeightUV;
	UInt height_minus1 = height - 1;

	cYUV<PixelType> *tempU = new cYUV<PixelType>(newWidth, this->m_uiHeightUV);
	cYUV<PixelType> *tempV = new cYUV<PixelType>(newWidth, this->m_uiHeightUV);

	switch ((Int)m_uiHorizontalPrecision) {
	case 2:
		for (UInt i = 0; i < this->m_uiWidthUV; i++) {
			leftPix = Clip3(i, 0, width_minus1);
			rightPix = Clip3(i + 1, 0, width_minus1);
			*Pix = i << 1;
			*(Pix + 1) = *Pix + 1;
			for (UInt j = 0; j < this->m_uiHeightUV; j++) {
				tempU->m_atY[j*newWidth + *Pix] = this->m_atU[j*m_uiWidthUV + leftPix];
				tempU->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atU[j*m_uiWidthUV + leftPix] + this->m_atU[j*m_uiWidthUV + rightPix] + 1) >> 1;

				tempV->m_atY[j*newWidth + *Pix] = this->m_atV[j*m_uiWidthUV + leftPix];
				tempV->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atV[j*m_uiWidthUV + leftPix] + this->m_atV[j*m_uiWidthUV + rightPix] + 1) >> 1;
			}
		}
		break;
	case 4:
		for (UInt i = 0; i < this->m_uiWidthUV; i++) {
			leftPix = Clip3(i, 0, width_minus1);
			rightPix = Clip3(i + 1, 0, width_minus1);
			*Pix = i << 2;
			*(Pix + 1) = *Pix + 1;
			*(Pix + 2) = *(Pix + 1) + 1;
			*(Pix + 3) = *(Pix + 2) + 1;
			for (UInt j = 0; j < this->m_uiHeightUV; j++) {
				tempU->m_atY[j*newWidth + *Pix] = this->m_atU[j*m_uiWidthUV + leftPix];
				tempU->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atU[j*m_uiWidthUV + leftPix] * 3 + this->m_atU[j*m_uiWidthUV + rightPix] + 2) >> 2;
				tempU->m_atY[j*newWidth + *(Pix + 2)] = (this->m_atU[j*m_uiWidthUV + leftPix] + this->m_atU[j*m_uiWidthUV + rightPix] + 1) >> 1;
				tempU->m_atY[j*newWidth + *(Pix + 3)] = (this->m_atU[j*m_uiWidthUV + leftPix] + this->m_atU[j*m_uiWidthUV + rightPix] * 3 + 2) >> 2;

				tempV->m_atY[j*newWidth + *Pix] = this->m_atV[j*m_uiWidthUV + leftPix];
				tempV->m_atY[j*newWidth + *(Pix + 1)] = (this->m_atV[j*m_uiWidthUV + leftPix] * 3 + this->m_atV[j*m_uiWidthUV + rightPix] + 2) >> 2;
				tempV->m_atY[j*newWidth + *(Pix + 2)] = (this->m_atV[j*m_uiWidthUV + leftPix] + this->m_atV[j*m_uiWidthUV + rightPix] + 1) >> 1;
				tempV->m_atY[j*newWidth + *(Pix + 3)] = (this->m_atV[j*m_uiWidthUV + leftPix] + this->m_atV[j*m_uiWidthUV + rightPix] * 3 + 2) >> 2;
			}
		}
		break;
	}
	delete this->m_atU;
	delete this->m_atV;
	this->m_atU = tempU->m_atY;
	this->m_atV = tempV->m_atY;
	this->m_uiWidthUV = newWidth;
	this->m_uiImSizeUV = newWidth * this->m_uiHeightUV*(m_uiBitsPerSample >> 3);
}

template <class PixelType>
Void cYUV<PixelType>::imResizeVerticalUV() {

	Int topPix, bottomPix;
	Int* Pix = new Int[(Int)m_uiVerticalPrecision];
	UInt newHeight = UInt(this->m_uiHeightUV*m_uiVerticalPrecision);
	UInt height_minus1 = this->m_uiHeightUV - 1;

	cYUV<PixelType> *tempU = new cYUV<PixelType>(this->m_uiWidthUV, newHeight);
	cYUV<PixelType> *tempV = new cYUV<PixelType>(this->m_uiWidthUV, newHeight);

	switch ((Int)m_uiVerticalPrecision) {
	case 2:
		for (UInt j = 0; j < this->m_uiHeightUV; j++) {
			topPix = Clip3(j, 0, height_minus1);
			bottomPix = Clip3(j + 1, 0, height_minus1);
			*Pix = j << 1;
			*(Pix + 1) = *Pix + 1;
			*(Pix + 2) = *(Pix + 1) + 1;
			*(Pix + 3) = *(Pix + 2) + 1;
			for (UInt i = 0; i < this->m_uiWidthUV; i++) {
				tempU->m_atY[*Pix * this->m_uiWidthUV + i] = this->m_atU[topPix * this->m_uiWidthUV + i];
				tempU->m_atY[*(Pix + 1) * this->m_uiWidthUV + i] = (this->m_atU[topPix * this->m_uiWidthUV + i] + this->m_atU[bottomPix * this->m_uiWidthUV + i] + 1) >> 1;
				tempV->m_atY[*Pix * this->m_uiWidthUV + i] = this->m_atV[topPix * this->m_uiWidthUV + i];
				tempV->m_atY[*(Pix + 1) * this->m_uiWidthUV + i] = (this->m_atV[topPix * this->m_uiWidthUV + i] + this->m_atV[bottomPix * this->m_uiWidthUV + i] + 1) >> 1;
			}
		}
		break;
	case 4:
		for (UInt j = 0; j < this->m_uiHeightUV; j++) {
			topPix = Clip3(j, 0, height_minus1);
			bottomPix = Clip3(j + 1, 0, height_minus1);
			*Pix = j << 2;
			*(Pix + 1) = *Pix + 1;
			*(Pix + 2) = *(Pix + 1) + 1;
			*(Pix + 3) = *(Pix + 2) + 1;
			for (UInt i = 0; i < this->m_uiWidthUV; i++) {
				tempU->m_atY[*Pix * this->m_uiWidthUV + i] = this->m_atU[topPix * this->m_uiWidthUV + i];
				tempU->m_atY[*(Pix + 1) * this->m_uiWidthUV + i] = (this->m_atU[topPix * this->m_uiWidthUV + i] * 3 + this->m_atU[bottomPix * this->m_uiWidthUV + i] + 2) >> 2;
				tempU->m_atY[*(Pix + 2) * this->m_uiWidthUV + i] = (this->m_atU[topPix * this->m_uiWidthUV + i] + this->m_atU[bottomPix * this->m_uiWidthUV + i] + 1) >> 1;
				tempU->m_atY[*(Pix + 3) * this->m_uiWidthUV + i] = (this->m_atU[topPix * this->m_uiWidthUV + i] + this->m_atU[bottomPix * this->m_uiWidthUV + i] * 3 + 2) >> 2;
				tempV->m_atY[*Pix * this->m_uiWidthUV + i] = this->m_atV[topPix * this->m_uiWidthUV + i];
				tempV->m_atY[*(Pix + 1) * this->m_uiWidthUV + i] = (this->m_atV[topPix * this->m_uiWidthUV + i] * 3 + this->m_atV[bottomPix * this->m_uiWidthUV + i] + 2) >> 2;
				tempV->m_atY[*(Pix + 2) * this->m_uiWidthUV + i] = (this->m_atV[topPix * this->m_uiWidthUV + i] + this->m_atV[bottomPix * this->m_uiWidthUV + i] + 1) >> 1;
				tempV->m_atY[*(Pix + 3) * this->m_uiWidthUV + i] = (this->m_atV[topPix * this->m_uiWidthUV + i] + this->m_atV[bottomPix * this->m_uiWidthUV + i] * 3 + 2) >> 2;
			}
		}
		break;
	}

	delete this->m_atU;
	delete this->m_atV;
	this->m_atU = tempU->m_atY;
	this->m_atV = tempV->m_atY;
	this->m_uiHeightUV = newHeight;
	this->m_uiImSizeUV = this->m_uiWidthUV*newHeight*(m_uiBitsPerSample >> 3);
}

template <class PixelType>
Void cYUV<PixelType>::imResize(Int filterType, Double hPrec, Double vPrec) {

	m_iFilterType = filterType;
	m_uiHorizontalPrecision = hPrec;
	m_uiVerticalPrecision = vPrec;

	if (m_uiHorizontalPrecision != 1) {
		imResizeHorizontalY();
		imResizeHorizontalUV();
	}
	if (m_uiVerticalPrecision != 1) {
		imResizeVerticalY();
		imResizeVerticalUV();
	}

	return;
}

template <class PixelType>
UInt cYUV<PixelType>::getChrominanceHorizontalShift() {
	if (m_uiChromaSubsampling == 420) {
		return 1;
	}
	if (m_uiChromaSubsampling == 444) {
		return 0;
	}

}

template <class PixelType>
UInt cYUV<PixelType>::getChrominanceVerticalShift() {
	if (m_uiChromaSubsampling == 420) {
		return 1;
	}
	if (m_uiChromaSubsampling == 444) {
		return 0;
	}
}

#endif