//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cSADPixelMatcher.h"

Void cSADPixelMatcher::init(cCfgParams &cfg) {
	cMatcher::init(cfg);
	m_dMaxError = OUT_OF_SCREEN;
	return;
}

Double cSADPixelMatcher::matchingError(cArray<cYUV<ImagePixelType>*> &rapcYUVInput, cArray<cSegmentation*> &rapcSegments, UInt uiCamId, UInt uiTargetCamId, Int uiX, Int uiY, Int uiXInTargetCam, Int uiYInTargetCam) {

	cYUV<ImagePixelType>* pcYUVSourceView = rapcYUVInput[uiCamId];
	cYUV<ImagePixelType>* pcYUVTargetView = rapcYUVInput[uiTargetCamId];

#if CHROMA_MATCH
	UInt uiChromaHShift = pcYUVSourceView->getChrominanceHorizontalShift();
	UInt uiChromaVShift = pcYUVSourceView->getChrominanceVerticalShift();
#endif

	UInt uiPosHinSourceView = uiY;
	UInt uiPosWinSourceView = uiX;
	UInt uiPixelPositionInSourceView = uiPosHinSourceView * m_uiWidth + uiPosWinSourceView;

	UInt uiPosHinTargetView = uiYInTargetCam;
	UInt uiPosWinTargetView = uiXInTargetCam;
	UInt uiPixelPositionInTargetView = uiPosHinTargetView * m_uiWidth + uiPosWinTargetView;

	Double dMatchingError = OUT_OF_SCREEN;

	if (uiPosWinTargetView >= 0 && uiPosWinTargetView < m_uiWidth &&
		uiPosHinTargetView >= 0 && uiPosHinTargetView < m_uiHeight) {
		dMatchingError = Abs(pcYUVTargetView->m_atY[uiPixelPositionInTargetView] - pcYUVSourceView->m_atY[uiPixelPositionInSourceView]);
#if GRADIENT_MATCH
		Int iLineUp = m_uiWidth;

		if (uiPosWinSourceView >= 1 && uiPosWinTargetView >= 1) {
			dMatchingError += Abs(pcYUVTargetView->m_atY[uiPixelPositionInTargetView] - pcYUVSourceView->m_atY[uiPixelPositionInSourceView] -
				pcYUVTargetView->m_atY[uiPixelPositionInTargetView - 1] +
				pcYUVSourceView->m_atY[uiPixelPositionInSourceView - 1]);
		}
		if (uiPosHinSourceView >= 1 && uiPosHinTargetView >= 1) {
			dMatchingError += Abs(pcYUVTargetView->m_atY[uiPixelPositionInTargetView] -
				pcYUVSourceView->m_atY[uiPixelPositionInSourceView] -
				pcYUVTargetView->m_atY[uiPixelPositionInTargetView - iLineUp] +
				pcYUVSourceView->m_atY[uiPixelPositionInSourceView - iLineUp]);
		}
#endif
#if CHROMA_MATCH
		UInt uiPosWinSourceViewUV = uiPosWinSourceView >> uiChromaHShift;
		UInt uiPosHinSourceViewUV = uiPosHinSourceView >> uiChromaVShift;
		UInt uiPixelPositionInSourceViewUV = uiPosHinSourceViewUV * (m_uiWidth >> uiChromaHShift) + uiPosWinSourceViewUV;

		UInt uiPosWinTargetViewUV = uiPosWinTargetView >> uiChromaHShift;
		UInt uiPosHinTargetViewUV = uiPosHinTargetView >> uiChromaVShift;
		UInt uiPixelPositionInTargetViewUV = uiPosHinTargetViewUV * (m_uiWidth >> uiChromaHShift) + uiPosWinTargetViewUV;

		dMatchingError += Abs(pcYUVTargetView->m_atU[uiPixelPositionInTargetViewUV] -
			pcYUVSourceView->m_atU[uiPixelPositionInSourceViewUV]);
		dMatchingError += Abs(pcYUVTargetView->m_atV[uiPixelPositionInTargetViewUV] -
			pcYUVSourceView->m_atV[uiPixelPositionInSourceViewUV]);
#endif
#if GRADIENT_MATCH && CHROMA_MATCH
		UInt iLineUpUV = (m_uiWidth >> uiChromaHShift);

		if (uiPosWinSourceViewUV >= 1 && uiPosWinTargetViewUV >= 1) {
			dMatchingError += Abs(pcYUVTargetView->m_atU[uiPixelPositionInTargetViewUV] -
				pcYUVSourceView->m_atU[uiPixelPositionInSourceViewUV] -
				pcYUVTargetView->m_atU[uiPixelPositionInTargetViewUV - 1] +
				pcYUVSourceView->m_atU[uiPixelPositionInSourceViewUV - 1]);
			dMatchingError += Abs(pcYUVTargetView->m_atV[uiPixelPositionInTargetViewUV] -
				pcYUVSourceView->m_atV[uiPixelPositionInSourceViewUV] -
				pcYUVTargetView->m_atV[uiPixelPositionInTargetViewUV - 1] +
				pcYUVSourceView->m_atV[uiPixelPositionInSourceViewUV - 1]);
		}
		if (uiPosHinSourceViewUV >= 1 && uiPosHinTargetViewUV >= 1) {
			dMatchingError += Abs(pcYUVTargetView->m_atU[uiPixelPositionInTargetViewUV] -
				pcYUVSourceView->m_atU[uiPixelPositionInSourceViewUV] -
				pcYUVTargetView->m_atU[uiPixelPositionInTargetViewUV - iLineUpUV] +
				pcYUVSourceView->m_atU[uiPixelPositionInSourceViewUV - iLineUpUV]);
			dMatchingError += Abs(pcYUVTargetView->m_atV[uiPixelPositionInTargetViewUV] -
				pcYUVSourceView->m_atV[uiPixelPositionInSourceViewUV] -
				pcYUVTargetView->m_atV[uiPixelPositionInTargetViewUV - iLineUpUV] +
				pcYUVSourceView->m_atV[uiPixelPositionInSourceViewUV - iLineUpUV]);
		}
#endif
	}

	return dMatchingError;
}