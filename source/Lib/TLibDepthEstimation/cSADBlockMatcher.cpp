//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cSADBlockMatcher.h"

Void cSADBlockMatcher::init(cCfgParams &cfg) {
	cSADPixelMatcher::init(cfg);

	m_uiBlockWidth = cfg.m_uiMatchingBlockWidth;
	m_uiBlockHeight = cfg.m_uiMatchingBlockHeight;

	return;
}

Double cSADBlockMatcher::matchingError(cArray<cYUV<ImagePixelType>*> &rapcYUVInput, cArray<cSegmentation*> &rapcSegments, UInt uiCamId, UInt uiTargetCamId, Int uiX, Int uiY, Int uiXInTargetCam, Int uiYInTargetCam) {
	Int iHalfBlockWidth = m_uiBlockWidth >> 1;
	Int iHalfBlockHeight = m_uiBlockHeight >> 1;

	UInt uiPosHinSourceView = uiY;
	UInt uiPosWinSourceView = uiX;
	UInt uiPixelPositionInSourceView = uiPosHinSourceView * m_uiWidth + uiPosWinSourceView;

	UInt uiPosHinTargetView = uiYInTargetCam;
	UInt uiPosWinTargetView = uiXInTargetCam;
	UInt uiPixelPositionInTargetView = uiPosHinTargetView * m_uiWidth + uiPosWinTargetView;

	Double dMatchingError = 0.0;
	UInt uiNumOfUsedCams = 0;
	for (Int ibX = -iHalfBlockWidth; ibX <= iHalfBlockWidth; ibX++) {
		for (Int ibY = -iHalfBlockHeight; ibY <= iHalfBlockHeight; ibY++) {
			if ((uiX + ibX) >= 0 && (uiX + ibX) <= m_uiWidth &&
				(uiY + ibY) >= 0 && (uiY + ibY) <= m_uiHeight &&
				(uiXInTargetCam + ibX) >= 0 && (uiXInTargetCam + ibX) <= m_uiWidth &&
				(uiYInTargetCam + ibY) >= 0 && (uiYInTargetCam + ibY) <= m_uiHeight) {
				dMatchingError += cSADPixelMatcher::matchingError(rapcYUVInput, rapcSegments, uiCamId, uiTargetCamId, (uiX + ibX), (uiY + ibY), (uiXInTargetCam + ibX), (uiYInTargetCam + ibY));
				uiNumOfUsedCams++;
			}
		}
	}
	if (uiNumOfUsedCams == 0)return OUT_OF_SCREEN;
	return dMatchingError / (Double)uiNumOfUsedCams;

}