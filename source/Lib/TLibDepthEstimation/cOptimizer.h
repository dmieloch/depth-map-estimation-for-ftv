//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __COPTIMIZER_H__
#define __COPTIMIZER_H__

#include "TLibCommon\TypeDef.h"
#include "paramsDE.h"
#include "TLibCommon\cArray.h"
#include "TLibDepthEstimation\cMatcher.h"
#include "TLibCommon\cYUV.h"
#include "TLibSegmentation\cSegmentation.h"

class cOptimizer {
public:
	UInt m_uiWidth;
	UInt m_uiHeight;
	UInt m_uiNumOfCams;
	UInt m_uiCenterCam;
	UInt m_uiImSize;
	UInt m_uiStartFrame;

	Double m_dSmoothingCoeff;

	Int m_iMatchNeighbors;
	UInt m_uiIFramePeriod;

	UInt m_uiNumOfLabels;

	UInt *m_auiLabel2Depth;
	Double *m_adLabel2Z;
	Double *m_adLabel2InvZ;

	cMatcher* m_pcMatcher;

	cOptimizer();
	virtual ~cOptimizer();

	virtual Void init(cCfgParams &cfg, cMatcher *matcher);
	virtual Void init(cCfgParams &rcCfg, cMatcher *pcMatcher, cArray<cSegmentation*> &rapcSegments) {};
	Void calcDepth(cArray<cYUV<DepthPixelType>*> &YUV_dm, cArray<UInt*> &rapuiDepthLabel, cArray<cSegmentation*> &rapcSegments);

	virtual Void optimize(cArray<cYUV<ImagePixelType>*> &rapcYUVInput, cArray<cCamParams<MatrixComputationalType>*> &rapcCameraParameters, cArray<UInt*> &rapuiDepthLabel, cArray<UInt*> &rapuiPrevDepthLabel, cArray<cYUV<ImagePixelType>*> &rapcYUVMask, cArray<cSegmentation*> &rapcSegments, UInt uiFrameId) = 0;
};

#endif