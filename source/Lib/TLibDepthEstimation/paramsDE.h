//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "TLibCommon/TypeDef.h"

#define DEPTH_BPS		   16	 // 8..8 bit 16..16 bit
#define VIDEO_BPS		   8	 // 8..8 bit

#define OUTPUT_YUV		  0
#define INPUT_YUV		   1

#define OUT_OF_SCREEN	   200

#define WTA_BEFORE_OPTIMIZATION 0

#define FAST_BLOCK_MATCHING 1
#define COLOR_GUIDED_FILTER 0

#define CFG_MIN_MAX_DEPTH   1

#define GLOBAL_LABELS	   1

#define LOCAL_LABELS_OUTPUT	1

#define OCC_PENALTY			0.1

#define ERRORS_TRUNC_THRESH 1000

#define GRADIENT_MATCH	  1				 
#define CHROMA_MATCH		1

#define INPUT_MASK		  0
//===============================================================

typedef UChar	   ImagePixelType;	 //Pixel Image Type 

#if DEPTH_BPS==8

typedef UChar	   DepthPixelType;
#define MAX_DEPTH   255

#elif DEPTH_BPS==16

typedef UShort	  DepthPixelType;
#define MAX_DEPTH   65535

#else

#error "Unsupported DEPTH_BPS"

#endif
