//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cOptimizer.h"

cOptimizer::cOptimizer() {
	return;
}

cOptimizer::~cOptimizer() {

}

Void cOptimizer::init(cCfgParams &cfg, cMatcher *pcMatcher) {

	m_uiWidth = cfg.m_uiSourceWidth;
	m_uiHeight = cfg.m_uiSourceHeight;
	m_uiImSize = m_uiWidth * m_uiHeight;
	m_uiNumOfCams = cfg.m_uiNumberOfCameras;
	m_uiCenterCam = pcMatcher->m_uiNumOfCams / 2;
	m_uiStartFrame = cfg.m_uiStartFrame;

	m_dSmoothingCoeff = cfg.m_dSmoothingCoefficient;
	m_iMatchNeighbors = cfg.m_iMatchNeighbors;
	m_uiIFramePeriod = cfg.m_uiIFramePeriod;

	m_uiNumOfLabels = cfg.m_uiNumberOfDepthSteps;
	m_auiLabel2Depth = pcMatcher->m_auiLabel2Depth;

	m_pcMatcher = pcMatcher;

	m_auiLabel2Depth = pcMatcher->m_auiLabel2Depth;
	m_adLabel2Z = pcMatcher->m_adLabel2Z;
	m_adLabel2InvZ = pcMatcher->m_adLabel2InvZ;

	return;
}

Void cOptimizer::calcDepth(cArray<cYUV<DepthPixelType>*> &rapcYUV, cArray<UInt*> &rapuiDepthLabel, cArray<cSegmentation*> &rapcSegments)
{

	for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++)
	{
		UInt* puiDepthLabel = rapuiDepthLabel[uiCamId];
		cSegmentation* pcSegments = rapcSegments[uiCamId];
		cYUV<DepthPixelType>* pcYUVDepth = rapcYUV[uiCamId];
		for (UInt uiPos = 0; uiPos < m_uiImSize; uiPos++)
		{
			if (0 && ((uiPos + 1) < m_uiImSize) && (uiPos > 0))
			{
				if ((pcSegments->m_cYUVlabel.m_atY[uiPos] == pcSegments->m_cYUVlabel.m_atY[uiPos + 1]) &&
					(pcSegments->m_cYUVlabel.m_atY[uiPos] == pcSegments->m_cYUVlabel.m_atY[uiPos - 1]))
				{
					pcYUVDepth->m_atY[uiPos] = (DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos]]];
				}
				else
				{
					if (pcSegments->m_cYUVlabel.m_atY[uiPos] != pcSegments->m_cYUVlabel.m_atY[uiPos - 1])
					{
						pcYUVDepth->m_atY[uiPos] = ((DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos]]] +
							(DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos - 1]]]) / 2;
					}
					else
					{
						pcYUVDepth->m_atY[uiPos] = ((DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos]]] +
							(DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos + 1]]]) / 2;
					}
				}
			}
			else
			{
				if (pcSegments->m_cYUVlabel.m_atY[uiPos] < pcSegments->m_uiNumOfSuperpix)
				{
					pcYUVDepth->m_atY[uiPos] = (DepthPixelType)m_auiLabel2Depth[puiDepthLabel[pcSegments->m_cYUVlabel.m_atY[uiPos]]];
				}
			}
		}
	}

	return;
}