//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cMatcher.h"

cMatcher::cMatcher() {
	m_adLabel2InvZ = NULL;
	m_adLabel2Z = NULL;
	m_auiLabel2Depth = NULL;
	return;
}

cMatcher::~cMatcher() {
	freeLabel2Depth();
	freeLabel2Z();
}

Void cMatcher::init(cCfgParams &cfg) {
	m_uiWidth = cfg.m_uiSourceWidth;
	m_uiHeight = cfg.m_uiSourceHeight;
	m_uiImSize = m_uiWidth * m_uiHeight;
	m_uiNumOfCams = cfg.m_uiNumberOfCameras;
	m_uiCenterCam = cfg.m_uiNumberOfCameras / 2;
	m_iMatchNeighbors = cfg.m_iMatchNeighbors;

	m_dNearestDepthValue = cfg.m_dNearestDepthValue;
	m_dFarthestDepthValue = cfg.m_dFarthestDepthValue;
	m_uiNumOfLabels = UInt(cfg.m_uiNumberOfDepthSteps);

#if CFG_MIN_MAX_DEPTH
	m_uiMinDepthValue = cfg.m_uiMinDepthValue;
	m_uiMaxDepthValue = cfg.m_uiMaxDepthValue;
	m_dInvZnearMinusInvZfarDivMaxDepth = (1.0 / m_dNearestDepthValue - 1.0 / m_dFarthestDepthValue) / Double(m_uiMaxDepthValue - m_uiMinDepthValue);
#else
	m_dInvZnearMinusInvZfarDivMaxDepth = (1.0 / m_dNearestDepthValue - 1.0 / m_dFarthestDepthValue) / MAX_DEPTH;
#endif

	m_dInvZfar = 1.0 / m_dFarthestDepthValue;

	initLabel2Depth();
	initLabel2Z();

	m_dMaxError = OUT_OF_SCREEN;

	return;
}

Void cMatcher::initLabel2Depth() {
	m_auiLabel2Depth = new UInt[m_uiNumOfLabels];

	for (UInt uiLabel = 0; uiLabel < m_uiNumOfLabels; uiLabel++) {
#if CFG_MIN_MAX_DEPTH
		m_auiLabel2Depth[uiLabel] = uiLabel * (m_uiMaxDepthValue - m_uiMinDepthValue) / (m_uiNumOfLabels - 1) + m_uiMinDepthValue + 0.5;
#else
		m_auiLabel2Depth[uiLabel] = uiLabel * MAX_DEPTH / (m_uiNumOfLabels - 1) + 0.5;
#endif
	}

	return;
}

Void cMatcher::initLabel2Z() {
	m_adLabel2InvZ = new Double[m_uiNumOfLabels];
	m_adLabel2Z = new Double[m_uiNumOfLabels];

	for (UInt uiLabel = 0; uiLabel < m_uiNumOfLabels; uiLabel++) {
#if CFG_MIN_MAX_DEPTH
		m_adLabel2InvZ[uiLabel] = (Double(m_auiLabel2Depth[uiLabel] - m_uiMinDepthValue) * m_dInvZnearMinusInvZfarDivMaxDepth + m_dInvZfar);
#else
		m_adLabel2InvZ[uiLabel] = (Double(m_auiLabel2Depth[uiLabel]) * m_dInvZnearMinusInvZfarDivMaxDepth + m_dInvZfar);
#endif
		m_adLabel2Z[uiLabel] = 1.0 / m_adLabel2InvZ[uiLabel];
	}

	return;
}

Void cMatcher::freeLabel2Depth() {
	if (m_auiLabel2Depth)
		delete m_auiLabel2Depth;
}

Void cMatcher::freeLabel2Z() {
	if (m_adLabel2Z)
		delete m_adLabel2Z;
	if (m_adLabel2InvZ)
		delete m_adLabel2InvZ;
}
