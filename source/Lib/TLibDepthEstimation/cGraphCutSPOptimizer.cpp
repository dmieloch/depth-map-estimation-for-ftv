//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cGraphCutSPOptimizer.h"
#include <time.h>
#include <omp.h>
#include <thread>

Void cGraphCutSPOptimizer::init(cCfgParams &rcCfg, cMatcher *pcMatcher, cArray<cSegmentation*> &rapcSegments) {
	cOptimizer::init(rcCfg, pcMatcher);

	m_iOcculsion = rcCfg.m_iOcclusion;
	m_uiSuperpixelSmoothing = rcCfg.m_uiSuperpixelSmoothing;
	m_dMatchThresh = rcCfg.m_dMatchThresh;
	m_uiTemporalEnhancementMethod = rcCfg.m_uiTemporalEnhancementMethod;
	m_uiNumOfCyclesInFirstFrame = rcCfg.m_uiNumOfCyclesInFirstFrame;
	m_uiNumOfCycles = rcCfg.m_uiNumOfCycles;
	m_uiNumOfThreads = rcCfg.m_uiNumOfThreads;
	m_uiParallelizationType = rcCfg.m_uiParallelizationType;
}

#define CrossCostLinear(a,b,c) (c*Abs(a-b))

Void cGraphCutSPOptimizer::optimize(cArray<cYUV<ImagePixelType>*> &rapcYUVInput, cArray<cCamParams<MatrixComputationalType>*> &rapcCameraParameters, cArray<UInt*> &rapuiDepthLabel, cArray<UInt*> &rapuiPrevDepthLabel, cArray<cYUV<ImagePixelType>*> &rapcYUVMask, cArray<cSegmentation*> &rapcSegments, UInt uiFrameId) {
	cMatrix<Double> **IPP = new cMatrix<Double>*[m_uiNumOfCams];
	cMatrix<Double> *PcIP = new cMatrix<Double>[m_uiNumOfCams];

	for (Int uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
		PcIP[uiCamId].init(4, 4);
		MatrixMultiply(rapcCameraParameters[m_uiNumOfCams / 2]->m_mProjMat, rapcCameraParameters[uiCamId]->m_mInvProjMat, PcIP[uiCamId]);
		IPP[uiCamId] = new cMatrix<Double>[m_uiNumOfCams];
		for (Int uiCamTargetId = 0; uiCamTargetId < m_uiNumOfCams; uiCamTargetId++) {
			IPP[uiCamId][uiCamTargetId].init(4, 4);
			MatrixMultiply(rapcCameraParameters[uiCamTargetId]->m_mProjMat, rapcCameraParameters[uiCamId]->m_mInvProjMat, IPP[uiCamId][uiCamTargetId]);
		}
	}

	cSegmentation* pcSegments;
	m_ppbNodesActive = new Bool**[m_uiNumOfThreads];
	m_ppcNodes = new Graph::node_id**[m_uiNumOfThreads];

	for (UInt uiTid = 0; uiTid < m_uiNumOfThreads; uiTid++) {
		m_ppbNodesActive[uiTid] = new Bool*[m_uiNumOfCams];
		m_ppcNodes[uiTid] = new Graph::node_id*[m_uiNumOfCams];

		for (UInt uiCam = 0; uiCam < m_uiNumOfCams; uiCam++) {
			m_ppbNodesActive[uiTid][uiCam] = new Bool[rapcSegments[uiCam]->m_uiNumOfSuperpix];
			m_ppcNodes[uiTid][uiCam] = new Graph::node_id[rapcSegments[uiCam]->m_uiNumOfSuperpix];

			for (UInt uiSPp = 0; uiSPp < rapcSegments[uiCam]->m_uiNumOfSuperpix; uiSPp++) {
				m_ppbNodesActive[uiTid][uiCam][uiSPp] = false;
				m_ppcNodes[uiTid][uiCam][uiSPp] = NULL;
			}
		}
	}

	UInt uiMaxCycle = m_uiNumOfCyclesInFirstFrame;
	if ((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod)uiMaxCycle = m_uiNumOfCycles;

	UInt Wm1 = m_uiWidth - 1;
	UInt Hm1 = m_uiHeight - 1;
	UInt uiImSize = m_uiWidth * m_uiHeight;

	cArray<cArray<UInt*>> rapuiDepthLabel_th;

	for (UInt uiTid = 0; uiTid < m_uiNumOfThreads; uiTid++) {
		cArray<UInt*> rapuiDepthLabel_temp;
		rapuiDepthLabel_th.push_back(rapuiDepthLabel_temp);

		for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
			UInt* rpuiDepthLabel_temp = new UInt[uiImSize];
			rapuiDepthLabel_th[uiTid].push_back(rpuiDepthLabel_temp);
		}
	}

	UInt uiLabelsPerThread = m_uiNumOfLabels / m_uiNumOfThreads;

	for (UInt uiTid = 0; uiTid < m_uiNumOfThreads; uiTid++) {
		UInt uiSource = Int(uiTid*(Double)m_uiNumOfLabels / (Double)m_uiNumOfThreads);
		UInt uiMax = Int((uiTid + 1)*m_uiNumOfLabels / (Double)m_uiNumOfThreads);
		for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
			for (UInt uiSPp = 0; uiSPp < rapcSegments[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
				if ((m_uiTemporalEnhancementMethod > 0) && (((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod)) > 0) {
					rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] = rapuiPrevDepthLabel[uiCamId][uiSPp];
					if (rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] >= m_uiNumOfLabels) {
						rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] = 0;
					}
				}
				else {
					rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] = uiTid * uiLabelsPerThread;
				}
			}
		}
	}

	cArray<std::thread> atThreads;
	for (UInt uiTid_temp = 0; uiTid_temp < m_uiNumOfThreads; uiTid_temp++) {

		atThreads.push_back(std::thread([&]() {
			UInt uiTid = uiTid_temp;
			Double dOldEnergy = DBL_MAX;

			for (UInt uiCycle = 0; uiCycle < uiMaxCycle; uiCycle++) {
				UInt uiSource, uiMax, uiInc;
				uiSource = Int(uiTid*(Double)m_uiNumOfLabels / (Double)m_uiNumOfThreads);
				uiMax = Int((uiTid + 1)*m_uiNumOfLabels / (Double)m_uiNumOfThreads);
				uiInc = 1;
				for (uiSource; uiSource < uiMax; uiSource += uiInc)
				{
					cSegmentation* pcSegments;
					// starting time
					clock_t clkTimeBegin, clkTimeEnd;
					clkTimeBegin = clock();

					fprintf(stdout, "Label %d, ", uiSource);
					Graph *kgGraph = new Graph();

					Double dEnergy = 0.0;
					for (UInt uiCam = 0; uiCam < m_uiNumOfCams; uiCam++)
					{
						pcSegments = rapcSegments[uiCam];
						for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++)
						{
							if (m_uiTemporalEnhancementMethod == 1)
							{
								if ((rapuiPrevDepthLabel[uiCam][uiSPp] < m_uiNumOfLabels) && ((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod))
								{
									m_ppcNodes[uiTid][uiCam][uiSPp] = NULL;
									m_ppbNodesActive[uiTid][uiCam][uiSPp] = false;
								}
								else
								{
									m_ppcNodes[uiTid][uiCam][uiSPp] = kgGraph->add_node();
									m_ppbNodesActive[uiTid][uiCam][uiSPp] = true;
								}
							}
							else
							{
								if (m_uiTemporalEnhancementMethod > 0)if (rapuiPrevDepthLabel[uiCam][uiSPp] >= m_uiNumOfLabels && rapuiDepthLabel_th[uiTid][uiCam][uiSPp] >= m_uiNumOfLabels)rapuiDepthLabel_th[uiTid][uiCam][uiSPp] -= m_uiNumOfLabels;
								m_ppcNodes[uiTid][uiCam][uiSPp] = kgGraph->add_node();
								m_ppbNodesActive[uiTid][uiCam][uiSPp] = true;
							}
						}
					}
					for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++)
					{
						pcSegments = rapcSegments[uiCamId];
						for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++)
						{
							UInt uiY = pcSegments->m_apiSuperpixel[uiSPp][4];
							UInt uiX = pcSegments->m_apiSuperpixel[uiSPp][3];
							UInt uiPosUV = (uiY >> 1)*(m_uiWidth >> 1) + (uiX >> 1);
							UInt uiLabel = rapuiDepthLabel_th[uiTid][uiCamId][uiSPp];
							Double dInvZ = m_adLabel2InvZ[uiLabel];
							Double dZ = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
							dInvZ = m_adLabel2InvZ[uiSource];
							Double dZSource = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
							for (Int uiSPnc = 0; uiSPnc < pcSegments->Superpixel_neigh.at(uiSPp).size(); uiSPnc++)
							{
								if (m_ppbNodesActive[uiTid][uiCamId][uiSPp])
								{
									UInt uiSPn = pcSegments->Superpixel_neigh.at(uiSPp).at(uiSPnc);
									if ((uiSPn < pcSegments->m_uiNumOfSuperpix) && (uiSPp < pcSegments->m_uiNumOfSuperpix))
									{
										UInt uiLabelPlusOne = rapuiDepthLabel_th[uiTid][uiCamId][uiSPn];
										dInvZ = m_adLabel2InvZ[uiLabelPlusOne];
										Double dZPlusOne = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / ((pcSegments->m_apiSuperpixel[uiSPn][3]) * PcIP[uiCamId].at(2, 0) + pcSegments->m_apiSuperpixel[uiSPn][4] * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
										dInvZ = m_adLabel2InvZ[uiSource];
										Double dZSourcePlusOne = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / ((pcSegments->m_apiSuperpixel[uiSPn][3]) * PcIP[uiCamId].at(2, 0) + pcSegments->m_apiSuperpixel[uiSPn][4] * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
										Double dSmoothingCoeff = m_dSmoothingCoeff;
										Double dSuperpixelDiff = 1;
										if (m_uiSuperpixelSmoothing) {
											dSuperpixelDiff =
												(
													Abs(rapcYUVInput[uiCamId]->m_atY[UInt(pcSegments->m_apiSuperpixel[uiSPp][4])*m_uiWidth + UInt(pcSegments->m_apiSuperpixel[uiSPp][3])] - rapcYUVInput[uiCamId]->m_atY[UInt(pcSegments->m_apiSuperpixel[uiSPn][4])*m_uiWidth + UInt(pcSegments->m_apiSuperpixel[uiSPn][3])]) +
													Abs(rapcYUVInput[uiCamId]->m_atU[(UInt(pcSegments->m_apiSuperpixel[uiSPp][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPp][3]) >> 1)] - rapcYUVInput[uiCamId]->m_atU[(UInt(pcSegments->m_apiSuperpixel[uiSPn][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPn][3]) >> 1)]) +
													Abs(rapcYUVInput[uiCamId]->m_atV[(UInt(pcSegments->m_apiSuperpixel[uiSPp][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPp][3]) >> 1)] - rapcYUVInput[uiCamId]->m_atV[(UInt(pcSegments->m_apiSuperpixel[uiSPn][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPn][3]) >> 1)])
													);
											dSuperpixelDiff = dSuperpixelDiff > 30 ? 30 : dSuperpixelDiff;
											dSmoothingCoeff = dSuperpixelDiff > 1 ? dSmoothingCoeff / dSuperpixelDiff : dSmoothingCoeff;
										}
										Double E00 = 0.0;
										Double E01 = 0.0;
										Double E10 = 0.0;
										Double E11 = 0.0;
										E00 = CrossCostLinear(dZ, dZPlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
										if (m_ppbNodesActive[uiTid][uiCamId][uiSPn])
											E01 = CrossCostLinear(dZ, dZSourcePlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
										E10 = CrossCostLinear(dZSource, dZPlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
										if (m_ppbNodesActive[uiTid][uiCamId][uiSPn])
											E11 = CrossCostLinear(dZSource, dZSourcePlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size()); //0;

										E00 *= 1000.0;
										E01 *= 1000.0;
										E10 *= 1000.0;
										E11 *= 1000.0;

										if (m_ppbNodesActive[uiTid][uiCamId][uiSPp])
										{
											if (m_ppbNodesActive[uiTid][uiCamId][uiSPn])
											{
												kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamId][uiSPn], E00, E01, E10, E11);
												dEnergy += (int)E00;
											}
											else
											{
												kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E00, E10);
											}
										}
									}
								}
							}
						}
					}

					cMatrix<Double> pix_dst(4, 1);
					for (Int uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++)
					{
						pcSegments = rapcSegments[uiCamId];
						for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++)
						{
							UInt uiY = pcSegments->m_apiSuperpixel[uiSPp][4];
							UInt uiX = pcSegments->m_apiSuperpixel[uiSPp][3];

							Double dInvZ = m_adLabel2InvZ[uiSource];
							dInvZ = (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2)) / (1 / dInvZ - PcIP[uiCamId].at(2, 3));

							UInt uiLabel = rapuiDepthLabel_th[uiTid][uiCamId][uiSPp];
							Double dInvZLabel = m_adLabel2InvZ[uiLabel];
							if (uiLabel != uiSource) {
								dInvZLabel = (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2)) / (1 / dInvZLabel - PcIP[uiCamId].at(2, 3));
							}
							Int iMatchNeighbors = ((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod) ? m_iMatchNeighbors : m_iMatchNeighbors;
							for (Int uiCamTargetId = Int(uiCamId - Int(iMatchNeighbors)); uiCamTargetId <= Int(uiCamId + Int(iMatchNeighbors)); uiCamTargetId++) {
								if ((uiCamTargetId < 0) || (uiCamTargetId == uiCamId) || (uiCamTargetId >= Int(m_uiNumOfCams))) continue;
								Double tmpw[3], tmph[3], tmp1[3];
								for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCamId][uiCamTargetId].at(i, 2);
								for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCamId][uiCamTargetId].at(i, 1) + tmp1[i];
								for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCamId][uiCamTargetId].at(i, 0) + tmph[i];
								if (uiLabel != uiSource)
								{
									for (Int i = 0; i < 3; i++) pix_dst.at(i, 0) = dInvZLabel * IPP[uiCamId][uiCamTargetId].at(i, 3) + tmpw[i];

									UInt uiPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
									UInt uiPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);
									if (uiPosWinTargetView >= 0 && uiPosWinTargetView < m_uiWidth && uiPosHinTargetView >= 0 && uiPosHinTargetView < m_uiHeight)
									{
										UInt uiSPpInTargetView = rapcSegments[uiCamTargetId]->m_cYUVlabel.m_atY[uiPosWinTargetView + uiPosHinTargetView * m_uiWidth];
										if (rapcSegments[uiCamTargetId]->m_uiNumOfSuperpix)
										{

											UInt uiTargetLabel = rapuiDepthLabel_th[uiTid][uiCamTargetId][uiSPpInTargetView];
											if ((uiTargetLabel == uiLabel) && (uiSPpInTargetView < pcSegments->m_uiNumOfSuperpix))
											{
												Double dME = m_pcMatcher->matchingError(rapcYUVInput, rapcSegments, uiCamId, uiCamTargetId, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
												Double Eaa = 0.0;
												Double E00 = Min(dME - m_dMatchThresh, 0);
												E00 *= 10.0;
												Double Ea0 = 0.0;
												Double E0a = 0.0;

												if (m_ppbNodesActive[uiTid][uiCamId][uiSPp])
												{
													if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView])
													{
														kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], E00, E0a, Ea0, Eaa);
														dEnergy += (int)E00;
													}
													else
													{
														kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E00, Ea0);
													}
												}
												else
												{
													if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView])
													{
														kgGraph->add_term1(m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], E00, E0a);
													}
												}
											}
										}
									}
								}

								{
									for (Int i = 0; i < 3; i++) {
										pix_dst.at(i, 0) = dInvZ * IPP[uiCamId][uiCamTargetId].at(i, 3) + tmpw[i];
									}

									UInt uiPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
									UInt uiPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);
									if (uiPosWinTargetView >= 0 && uiPosWinTargetView < m_uiWidth && uiPosHinTargetView >= 0 && uiPosHinTargetView < m_uiHeight)
									{
										UInt uiSPpInTargetView = rapcSegments[uiCamTargetId]->m_cYUVlabel.m_atY[uiPosWinTargetView + uiPosHinTargetView * m_uiWidth];
										if (uiSPpInTargetView < rapcSegments[uiCamTargetId]->m_uiNumOfSuperpix)
										{
											UInt uiTargetLabel = rapuiDepthLabel_th[uiTid][uiCamTargetId][uiSPpInTargetView];
											Double E0a = 0.0;
											Double Ea0 = 0.0;
											Double dME = m_pcMatcher->matchingError(rapcYUVInput, rapcSegments, uiCamId, uiCamTargetId, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
											Double Eaa = Min(dME - m_dMatchThresh, 0);
											Eaa *= 10.0;
											if (m_ppbNodesActive[uiTid][uiCamId][uiSPp])
											{
												if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView])
												{
													kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], 0, E0a, Ea0, Eaa);
												}
												else
												{
													if (uiTargetLabel == uiSource)
													{
														kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E0a, Eaa);
													}
												}
											}
											else
											{
												if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView])
												{
													if (uiLabel == uiSource)
													{
														kgGraph->add_term1(m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], Ea0, Eaa);
													}
												}
											}


										}
									}
								}
							}
						}
					}
					Graph::flowtype flow = kgGraph->maxflow();
					dEnergy += flow;
					if (dEnergy < dOldEnergy) {
						for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
							pcSegments = rapcSegments[uiCamId];
							for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++) {
								if ((m_ppbNodesActive[uiTid][uiCamId][uiSPp]))
								{
									if (kgGraph->what_segment(m_ppcNodes[uiTid][uiCamId][uiSPp]) != Graph::SOURCE)
									{
										rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] = uiSource;
									}
								}
							}
						}
						dOldEnergy = dEnergy;
					}
					delete kgGraph;

					pix_dst.~cMatrix();
					clkTimeEnd = clock();
				}
			}

		}
		));
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}

	for (UInt uiTid2 = 0; uiTid2 < m_uiNumOfThreads; uiTid2++) atThreads[uiTid2].join();
	atThreads.clear();
	cArray<std::thread> atThreads2;
	UInt ui2Pow = 2;

	for (UInt uiNumOfIter = 0; uiNumOfIter < m_uiNumOfThreads / 2; uiNumOfIter++) {
		for (UInt uiTid_temp = 0; uiTid_temp < m_uiNumOfThreads; uiTid_temp += ui2Pow) {
			atThreads2.push_back(std::thread([&]()
			{
				UInt uiTid = uiTid_temp;
				if ((uiTid + ui2Pow / 2) < m_uiNumOfThreads) {
					Double dOldEnergy = DBL_MAX;
					for (UInt uiCycle = 0; uiCycle < 2; uiCycle++) {
						clock_t clkTimeBegin, clkTimeEnd;
						clkTimeBegin = clock();

						fprintf(stdout, "merging ");
						Graph *kgGraph = new Graph();
						cSegmentation* pcSegments;
						Double dEnergy = 0.0;

						for (UInt uiCam = 0; uiCam < m_uiNumOfCams; uiCam++) {
							pcSegments = rapcSegments[uiCam];

							for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++) {
								if (m_uiTemporalEnhancementMethod == 1) {
									if ((rapuiPrevDepthLabel[uiCam][uiSPp] < m_uiNumOfLabels) && ((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod)) {
										m_ppcNodes[uiTid][uiCam][uiSPp] = NULL;
										m_ppbNodesActive[uiTid][uiCam][uiSPp] = false;
									}
									else {
										m_ppcNodes[uiTid][uiCam][uiSPp] = kgGraph->add_node();
										m_ppbNodesActive[uiTid][uiCam][uiSPp] = true;
									}
								}
								else {
									if (m_uiTemporalEnhancementMethod > 0)if (rapuiPrevDepthLabel[uiCam][uiSPp] >= m_uiNumOfLabels && rapuiDepthLabel_th[uiTid][uiCam][uiSPp] >= m_uiNumOfLabels)rapuiDepthLabel_th[uiTid][uiCam][uiSPp] -= m_uiNumOfLabels;
									m_ppcNodes[uiTid][uiCam][uiSPp] = kgGraph->add_node();
									m_ppbNodesActive[uiTid][uiCam][uiSPp] = true;
								}

							}
						}
						for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {

							pcSegments = rapcSegments[uiCamId];

							for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++) {
								UInt uiY = pcSegments->m_apiSuperpixel[uiSPp][4];
								UInt uiX = pcSegments->m_apiSuperpixel[uiSPp][3];
								UInt uiPosUV = (uiY >> 1)*(m_uiWidth >> 1) + (uiX >> 1);
								UInt uiLabel = rapuiDepthLabel_th[uiTid][uiCamId][uiSPp];
								Double dInvZ = m_adLabel2InvZ[uiLabel];
								Double dZ = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
								UInt uiSource = rapuiDepthLabel_th[uiTid + ui2Pow / 2][uiCamId][uiSPp];
								dInvZ = m_adLabel2InvZ[uiSource];
								Double dZSource = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
								for (Int uiSPnc = 0; uiSPnc < pcSegments->Superpixel_neigh[uiSPp].size(); uiSPnc++) {
									if (m_ppbNodesActive[uiTid][uiCamId][uiSPp]) {
										UInt uiSPn = pcSegments->Superpixel_neigh[uiSPp][uiSPnc];
										if ((uiSPn < pcSegments->m_uiNumOfSuperpix) && (uiSPp < pcSegments->m_uiNumOfSuperpix)) {
											UInt uiLabelPlusOne = rapuiDepthLabel_th[uiTid][uiCamId][uiSPn];
											dInvZ = m_adLabel2InvZ[uiLabelPlusOne];
											Double dZPlusOne = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / ((pcSegments->m_apiSuperpixel[uiSPn][3]) * PcIP[uiCamId].at(2, 0) + pcSegments->m_apiSuperpixel[uiSPn][4] * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
											dInvZ = m_adLabel2InvZ[uiSource];
											Double dZSourcePlusOne = (1 / dInvZ - PcIP[uiCamId].at(2, 3)) / ((pcSegments->m_apiSuperpixel[uiSPn][3]) * PcIP[uiCamId].at(2, 0) + pcSegments->m_apiSuperpixel[uiSPn][4] * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2));
											Double dSmoothingCoeff = m_dSmoothingCoeff;
											Double dSuperpixelDiff = 1;
											if (m_uiSuperpixelSmoothing) {
												dSuperpixelDiff =
													(
														Abs(rapcYUVInput[uiCamId]->m_atY[UInt(pcSegments->m_apiSuperpixel[uiSPp][4])*m_uiWidth + UInt(pcSegments->m_apiSuperpixel[uiSPp][3])] - rapcYUVInput[uiCamId]->m_atY[UInt(pcSegments->m_apiSuperpixel[uiSPn][4])*m_uiWidth + UInt(pcSegments->m_apiSuperpixel[uiSPn][3])]) +
														Abs(rapcYUVInput[uiCamId]->m_atU[(UInt(pcSegments->m_apiSuperpixel[uiSPp][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPp][3]) >> 1)] - rapcYUVInput[uiCamId]->m_atU[(UInt(pcSegments->m_apiSuperpixel[uiSPn][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPn][3]) >> 1)]) +
														Abs(rapcYUVInput[uiCamId]->m_atV[(UInt(pcSegments->m_apiSuperpixel[uiSPp][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPp][3]) >> 1)] - rapcYUVInput[uiCamId]->m_atV[(UInt(pcSegments->m_apiSuperpixel[uiSPn][4]) >> 1)*(m_uiWidth >> 1) + (UInt(pcSegments->m_apiSuperpixel[uiSPn][3]) >> 1)])
														);
												dSuperpixelDiff = dSuperpixelDiff > 30 ? 30 : dSuperpixelDiff;
												dSmoothingCoeff = dSuperpixelDiff > 1 ? dSmoothingCoeff / dSuperpixelDiff : dSmoothingCoeff;
											}

											Double E00 = 0.0;
											Double E01 = 0.0;
											Double E10 = 0.0;
											Double E11 = 0.0;

											E00 = CrossCostLinear(dZ, dZPlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
											if (m_ppbNodesActive[uiTid][uiCamId][uiSPn])
												E01 = CrossCostLinear(dZ, dZSourcePlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
											E10 = CrossCostLinear(dZSource, dZPlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());
											if (m_ppbNodesActive[uiTid][uiCamId][uiSPn])
												E11 = CrossCostLinear(dZSource, dZSourcePlusOne, dSmoothingCoeff) / (Double)(pcSegments->Superpixel_neigh.at(uiSPp).size());

											E00 *= 1000.0;
											E01 *= 1000.0;
											E10 *= 1000.0;
											E11 *= 1000.0;

											if (m_ppbNodesActive[uiTid][uiCamId][uiSPp]) {
												if (m_ppbNodesActive[uiTid][uiCamId][uiSPn]) {
													kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamId][uiSPn], E00, E01, E10, E11);
												}
												else {
													kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E00, E10);
												}
											}
										}
									}
								}
							}
						}

						cMatrix<Double> pix_dst(4, 1);
						for (Int uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
							pcSegments = rapcSegments[uiCamId];
							for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++) {
								UInt uiY = pcSegments->m_apiSuperpixel[uiSPp][4];
								UInt uiX = pcSegments->m_apiSuperpixel[uiSPp][3];
								UInt uiSource = rapuiDepthLabel_th[uiTid + ui2Pow / 2][uiCamId][uiSPp];
								if ((m_uiTemporalEnhancementMethod == 1) && (((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod)) && rapuiPrevDepthLabel[uiCamId][uiSPp] < m_uiNumOfLabels)uiSource = 0;
								Double dInvZ = m_adLabel2InvZ[uiSource];
								dInvZ = (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2)) / (1 / dInvZ - PcIP[uiCamId].at(2, 3));
								UInt uiLabel = rapuiDepthLabel_th[uiTid][uiCamId][uiSPp];
								Double dInvZLabel = m_adLabel2InvZ[uiLabel];
								if (uiLabel != uiSource) {
									dInvZLabel = (uiX * PcIP[uiCamId].at(2, 0) + uiY * PcIP[uiCamId].at(2, 1) + PcIP[uiCamId].at(2, 2)) / (1 / dInvZLabel - PcIP[uiCamId].at(2, 3));
								}

								Int iMatchNeighbors = ((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod) ? m_iMatchNeighbors : m_iMatchNeighbors;
								for (Int uiCamTargetId = Int(uiCamId - Int(iMatchNeighbors)); uiCamTargetId <= Int(uiCamId + Int(iMatchNeighbors)); uiCamTargetId++) {
									if ((uiCamTargetId < 0) || (uiCamTargetId == uiCamId) || (uiCamTargetId >= Int(m_uiNumOfCams))) continue;
									Double tmpw[3], tmph[3], tmp1[3];
									for (Int i = 0; i < 3; i++) tmp1[i] = IPP[uiCamId][uiCamTargetId].at(i, 2);
									for (Int i = 0; i < 3; i++) tmph[i] = uiY * IPP[uiCamId][uiCamTargetId].at(i, 1) + tmp1[i];
									for (Int i = 0; i < 3; i++) tmpw[i] = uiX * IPP[uiCamId][uiCamTargetId].at(i, 0) + tmph[i];
									if (uiLabel != uiSource)
									{
										for (Int i = 0; i < 3; i++) pix_dst.at(i, 0) = dInvZLabel * IPP[uiCamId][uiCamTargetId].at(i, 3) + tmpw[i];

										UInt uiPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
										UInt uiPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);

										if (uiPosWinTargetView >= 0 && uiPosWinTargetView < m_uiWidth &&
											uiPosHinTargetView >= 0 && uiPosHinTargetView < m_uiHeight) {
											UInt uiSPpInTargetView = rapcSegments[uiCamTargetId]->m_cYUVlabel.m_atY[uiPosWinTargetView + uiPosHinTargetView * m_uiWidth];
											if (rapcSegments[uiCamTargetId]->m_uiNumOfSuperpix) {
												UInt uiTargetLabel = rapuiDepthLabel_th[uiTid][uiCamTargetId][uiSPpInTargetView];
												if ((uiTargetLabel == uiLabel) && (uiSPpInTargetView < pcSegments->m_uiNumOfSuperpix)) {
													Double dME = m_pcMatcher->matchingError(rapcYUVInput, rapcSegments, uiCamId, uiCamTargetId, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
													Double Eaa = 0.0;
													Double E00 = Min(dME - m_dMatchThresh, 0);
													E00 *= 10.0;
													Double Ea0 = 0.0;
													Double E0a = 0.0;
													if (m_ppbNodesActive[uiTid][uiCamId][uiSPp]) {
														if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView]) {
															kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], E00, E0a, Ea0, Eaa);
															dEnergy += (int)E00;

														}
														else {
															kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E00, Ea0);
														}
													}
													else {
														if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView]) {
															kgGraph->add_term1(m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], E00, E0a);

														}
													}
												}
											}
										}
									}
									{
										for (Int i = 0; i < 3; i++) {
											pix_dst.at(i, 0) = dInvZ * IPP[uiCamId][uiCamTargetId].at(i, 3) + tmpw[i];
										}

										UInt uiPosWinTargetView = int(pix_dst.at(0, 0) / pix_dst.at(2, 0) + 0.5);
										UInt uiPosHinTargetView = int(pix_dst.at(1, 0) / pix_dst.at(2, 0) + 0.5);

										if (uiPosWinTargetView >= 0 && uiPosWinTargetView < m_uiWidth &&
											uiPosHinTargetView >= 0 && uiPosHinTargetView < m_uiHeight) {
											UInt uiSPpInTargetView = rapcSegments[uiCamTargetId]->m_cYUVlabel.m_atY[uiPosWinTargetView + uiPosHinTargetView * m_uiWidth];
											if (uiSPpInTargetView < rapcSegments[uiCamTargetId]->m_uiNumOfSuperpix) {
												UInt uiTargetLabel = rapuiDepthLabel_th[uiTid][uiCamTargetId][uiSPpInTargetView];
												Double E0a = 0.0;
												Double Ea0 = 0.0;
												Double dME = m_pcMatcher->matchingError(rapcYUVInput, rapcSegments, uiCamId, uiCamTargetId, uiX, uiY, uiPosWinTargetView, uiPosHinTargetView);
												Double Eaa = Min(dME - m_dMatchThresh, 0);
												Eaa *= 10.0;
												if (m_ppbNodesActive[uiTid][uiCamId][uiSPp]) {
													if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView]) {
														kgGraph->add_term2(m_ppcNodes[uiTid][uiCamId][uiSPp], m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], 0, E0a, Ea0, Eaa);
													}
													else {
														if (uiTargetLabel == uiSource) {
															kgGraph->add_term1(m_ppcNodes[uiTid][uiCamId][uiSPp], E0a, Eaa);
														}
													}
												}
												else {
													if (m_ppbNodesActive[uiTid][uiCamTargetId][uiSPpInTargetView]) {
														if (uiLabel == uiSource) {
															kgGraph->add_term1(m_ppcNodes[uiTid][uiCamTargetId][uiSPpInTargetView], Ea0, Eaa);
														}
													}
												}


											}
										}
									}
								}
							}
						}
						Graph::flowtype flow = kgGraph->maxflow();
						dEnergy += flow;
						if (dEnergy < dOldEnergy) {
							for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
								pcSegments = rapcSegments[uiCamId];
								for (UInt uiSPp = 0; uiSPp < pcSegments->m_uiNumOfSuperpix; uiSPp++) {
#if INPUT_MASK
									if (rapcYUVMask[uiCamId]->m_atY[uiPos] == 0) {
										continue;
									}
#endif
									if ((m_ppbNodesActive[uiTid][uiCamId][uiSPp])) {
										if (kgGraph->what_segment(m_ppcNodes[uiTid][uiCamId][uiSPp]) != Graph::SOURCE) {
											UInt uiSource = rapuiDepthLabel_th[uiTid + ui2Pow / 2][uiCamId][uiSPp];
											rapuiDepthLabel_th[uiTid][uiCamId][uiSPp] = uiSource;
										}
									}
								}

							}
							dOldEnergy = dEnergy;
						}
						delete kgGraph;

						pix_dst.~cMatrix();

						clkTimeEnd = clock();
					}
				}
			}
			));
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
		}

		for (UInt uiTid2 = 0; uiTid2 < atThreads2.size(); uiTid2++) atThreads2[uiTid2].join();
		atThreads2.clear();
		ui2Pow *= 2;
	}

	for (Int uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
		PcIP[uiCamId].~cMatrix();
		for (Int uiCamTargetId = 0; uiCamTargetId < m_uiNumOfCams; uiCamTargetId++) {
			IPP[uiCamId][uiCamTargetId].~cMatrix();

		}
		delete[] IPP[uiCamId];
	}



	for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
		for (UInt uiSPp = 0; uiSPp < rapcSegments[uiCamId]->m_uiNumOfSuperpix; uiSPp++) {
			if ((m_uiTemporalEnhancementMethod == 1) && (((uiFrameId - m_uiStartFrame) % m_uiIFramePeriod)) && (rapuiPrevDepthLabel[uiCamId][uiSPp] < m_uiNumOfLabels)) rapuiDepthLabel[uiCamId][uiSPp] = rapuiPrevDepthLabel[uiCamId][uiSPp];
			else rapuiDepthLabel[uiCamId][uiSPp] = rapuiDepthLabel_th[0][uiCamId][uiSPp];
		}
	}

	for (UInt uiTid = 0; uiTid < m_uiNumOfThreads; uiTid++) {
		for (UInt uiCamId = 0; uiCamId < m_uiNumOfCams; uiCamId++) {
			delete[] rapuiDepthLabel_th[uiTid][uiCamId];
		}
		rapuiDepthLabel_th[uiTid].clear();
	}
	rapuiDepthLabel_th.clear();

	for (UInt uiTid = 0; uiTid < m_uiNumOfThreads; uiTid++) {

		for (UInt uiCam = 0; uiCam < m_uiNumOfCams; uiCam++) {

			if (m_ppbNodesActive[uiTid][uiCam]) delete[] m_ppbNodesActive[uiTid][uiCam];
			if (m_ppcNodes[uiTid][uiCam]) delete[] m_ppcNodes[uiTid][uiCam];
		}

		if (m_ppbNodesActive[uiTid]) delete[] m_ppbNodesActive[uiTid];
		if (m_ppcNodes[uiTid]) delete[] m_ppcNodes[uiTid];
	}

	return;
}