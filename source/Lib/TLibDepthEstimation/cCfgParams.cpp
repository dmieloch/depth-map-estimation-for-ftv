//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cCfgParams.h"

#include <iostream>
#include <fstream>
#include <string.h>

cCfgParams::cCfgParams(Char cfgFilename[]) {

	FILE *CfgFile = NULL;
	fopen_s(&CfgFile, cfgFilename, "r");

	if (!CfgFile) {
		fprintf(stdout, "Configuration file %s doesn't exist\n", cfgFilename);
		system("PAUSE");
	}

	//Default Value
	m_uiMaxDepthValue = MAX_DEPTH;
	m_uiMinDepthValue = 0;
	m_uiNumOfThreads = 1;
	m_uiChrominanceFormat = 420;
	m_uiNumOfSuperpixels = 1000;
	m_dSuperpixelColorCoeff = 1;
	m_uiSuperpixelSmoothing = 1;
	m_uiTemporalEnhancementMethod = 1;
	m_uiIFramePeriod = 50;
	m_uiNumOfCyclesInFirstFrame = 1;
	m_uiNumOfCycles = 1;
	m_iMatchNeighbors = 1;
	m_dMatchThresh = 20;
	m_dTempThresh = 2.0;
	m_uiHorizontalPrecision = 1;
	m_uiVerticalPrecision = 1;
	m_iMatchingMethod = 0;
	m_iOptimizationMethod = 1;
	///////////////


	Char tmpPar[MAX_TEXT_LENGTH];
	Char tmpVal[MAX_TEXT_LENGTH];

	Char tmpDigits[11];
	strcpy_s(tmpDigits, 11, "0123456789");
	int tmpCamNum = 0;

	m_acCameraNames.clear();
	m_acFileInputViews.clear();

	while (!feof(CfgFile)) {
		fscanf_s(CfgFile, "%s", tmpPar, MAX_TEXT_LENGTH);
		if (feof(CfgFile)) break;
		if (tmpPar[0] == '#') fgets(tmpPar, MAX_TEXT_LENGTH, CfgFile);
		else {
			fscanf_s(CfgFile, "%s", tmpVal, MAX_TEXT_LENGTH);

			int digitpos = (int)strcspn(tmpPar, tmpDigits);
			int digits = (int)strlen(tmpPar) - digitpos;

			if (!strncmp(tmpPar, "NameOfCamera", digitpos)) m_acCameraNames.push_back(tmpVal);
			if (!strncmp(tmpPar, "InputView", digitpos)) m_acFileInputViews.push_back(tmpVal);
			if (!strncmp(tmpPar, "InputMask", digitpos)) m_acFileInputMasks.push_back(tmpVal);
			if (!strncmp(tmpPar, "OutputDepthMap", digitpos)) m_acFileOutputDepthMaps.push_back(tmpVal);
			if (!strncmp(tmpPar, "OutputErrors", digitpos)) m_acFileOutputErrors.push_back(tmpVal);

		}

	}

	fseek(CfgFile, 0, 0);

	while (!feof(CfgFile)) {

		fscanf_s(CfgFile, "%s", tmpPar, MAX_TEXT_LENGTH);
		if (feof(CfgFile)) break;
		if (tmpPar[0] == '#')
			fgets(tmpPar, MAX_TEXT_LENGTH, CfgFile);
		else {
			fscanf_s(CfgFile, "%s", tmpVal, MAX_TEXT_LENGTH);

			if (!strcmp(tmpPar, "NumOfThreads")) m_uiNumOfThreads = atoi(tmpVal);
			if (!strcmp(tmpPar, "ParallelizationType")) m_uiParallelizationType = atoi(tmpVal);
			if (!strcmp(tmpPar, "SourceWidth")) m_uiSourceWidth = atoi(tmpVal);
			if (!strcmp(tmpPar, "SourceHeight")) m_uiSourceHeight = atoi(tmpVal);
			if (!strcmp(tmpPar, "StartFrame")) m_uiStartFrame = atoi(tmpVal);
			if (!strcmp(tmpPar, "TotalNumberOfFrames")) m_uiTotalNumberOfFrames = atoi(tmpVal);
			if (!strcmp(tmpPar, "ChrominanceFormat")) m_uiChrominanceFormat = atoi(tmpVal);
			if (!strcmp(tmpPar, "FileCameraParameter"))	strcpy_s(m_sFileCameraParameter, tmpVal);
			if (!strcmp(tmpPar, "NearestZValue")) m_dNearestDepthValue = atof(tmpVal);
			if (!strcmp(tmpPar, "FarthestZValue")) m_dFarthestDepthValue = atof(tmpVal);
			if (!strcmp(tmpPar, "NumberOfZSteps")) m_uiNumberOfDepthSteps = atoi(tmpVal);
			if (!strcmp(tmpPar, "MinDepthValue")) m_uiMinDepthValue = (UInt)atof(tmpVal);
			if (!strcmp(tmpPar, "MaxDepthValue")) m_uiMaxDepthValue = (UInt)atof(tmpVal);
			if (!strcmp(tmpPar, "HorizontalPrecision")) m_uiHorizontalPrecision = atoi(tmpVal);
			if (!strcmp(tmpPar, "VerticalPrecision")) m_uiVerticalPrecision = atoi(tmpVal);
			if (!strcmp(tmpPar, "FilterType")) m_iFilterType = atoi(tmpVal);
			if (!strcmp(tmpPar, "MatchingMethod")) m_iMatchingMethod = atoi(tmpVal);
			if (!strcmp(tmpPar, "OptimizationMethod")) m_iOptimizationMethod = atoi(tmpVal);
			if (!strcmp(tmpPar, "MatchNeighbors")) m_iMatchNeighbors = atoi(tmpVal);
			if (!strcmp(tmpPar, "MatchThresh")) m_dMatchThresh = atoi(tmpVal);
			if (!strcmp(tmpPar, "MatchingBlockWidth")) m_uiMatchingBlockWidth = atoi(tmpVal);
			if (!strcmp(tmpPar, "MatchingBlockHeight")) m_uiMatchingBlockHeight = atoi(tmpVal);
			if (!strcmp(tmpPar, "SmoothingCoefficient")) m_dSmoothingCoefficient = atof(tmpVal);
			if (!strcmp(tmpPar, "SuperpixelSmoothing")) m_uiSuperpixelSmoothing = atoi(tmpVal);
			if (!strcmp(tmpPar, "SuperpixelColorCoeff")) m_dSuperpixelColorCoeff = atof(tmpVal);
			if (!strcmp(tmpPar, "NumOfSuperpixels")) m_uiNumOfSuperpixels = atoi(tmpVal);
			if (!strcmp(tmpPar, "TemporalEnhancementMethod")) m_uiTemporalEnhancementMethod = atoi(tmpVal);
			if (!strcmp(tmpPar, "IFramePeriod")) m_uiIFramePeriod = atoi(tmpVal);
			if (!strcmp(tmpPar, "NumOfCyclesInFirstFrame")) m_uiNumOfCyclesInFirstFrame = atoi(tmpVal);
			if (!strcmp(tmpPar, "NumOfCycles")) m_uiNumOfCycles = atoi(tmpVal);
			if (!strcmp(tmpPar, "TempThresh")) m_dTempThresh = atof(tmpVal);

			int digitpos = (int)strcspn(tmpPar, tmpDigits);
			int digits = (int)strlen(tmpPar) - digitpos;

			int camnum;
			if (digits == 1) camnum = (int(tmpPar[digitpos]) - 48);
			if (digits == 2) camnum = (int(tmpPar[digitpos]) - 48) * 10 + (int(tmpPar[digitpos + 1]) - 48);

			if (!strncmp(tmpPar, "NameOfCamera", digitpos)) m_acCameraNames[camnum] = tmpVal;
			if (!strncmp(tmpPar, "InputView", digitpos)) m_acFileInputViews[camnum] = tmpVal;
			if (!strncmp(tmpPar, "OutputDepthMap", digitpos)) m_acFileOutputDepthMaps[camnum] = tmpVal;

		}
	}

	m_dSmoothingCoefficient /= m_uiHorizontalPrecision;

	m_uiNumberOfCameras = (UInt)m_acCameraNames.size();
	m_uiCenterCam = m_uiNumberOfCameras / 2;

	fclose(CfgFile);

	return;
}
