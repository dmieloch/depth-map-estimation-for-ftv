//=================================================================================
//
//  AUTORIGHTS
//	Depth map estimation for free-viewpoint television and virtual navigation
//  Copyright (C) 2019 Poznan University of Technology
//
//  Corresponding author: Dawid Mieloch (dawid.mieloch@put.poznan.pl)
//
//  Code released for research purposes. 
//	If you use this software, please cite the corresponding article: 
//	"Depth Map Estimation for Free-Viewpoint Television and Virtual Navigation"
//	(doi: 10.1109/ACCESS.2019.2963487)
//	
//  For commercial purposes, please contact the author.
//=================================================================================

/*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met
*This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of PUT nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __CLOCALIZER_H__
#define __CLOCALIZER_H__

#include "TLibCommon\TypeDef.h"
#include "TLibCommon\cArray.h"
#include "TLibCommon\cCamParams.h"
#include "TLibDepthEstimation\cCfgParams.h"
#include "TLibCommon\cYUV.h"
#include "TLibCommon\cMatrix.h"
#include "TLibDepthEstimation\paramsDE.h"

class cLocalizer {
public:
	cCamParams<Double> *m_pCamVParams;
	cCamParams<Double> **m_apCamRParams;

	Double m_dZfar;
	Double m_dZnear;

	UInt m_uiNumOfCams;
	UInt m_uiCenterCam;

	Void init(cArray<cCamParams<Double>*> &camparams, cCfgParams &cfg);
	Void localize(cArray<cYUV<DepthPixelType>*> &apcYUVinputDepth, UInt uiVCam, cCfgParams cParams);

};


Void cLocalizer::init(cArray<cCamParams<Double>*> &camparams, cCfgParams &cfg) {

	m_uiNumOfCams = cfg.m_uiNumberOfCameras;
	m_uiCenterCam = cfg.m_uiCenterCam;

	m_dZfar = cfg.m_dFarthestDepthValue;
	m_dZnear = cfg.m_dNearestDepthValue;

	m_apCamRParams = new cCamParams<Double>*[m_uiNumOfCams + 1];
	m_pCamVParams = new cCamParams<Double>;

	m_apCamRParams[m_uiCenterCam] = camparams[m_uiCenterCam];

	for (UInt c = 0; c < m_uiNumOfCams; c++) {
		m_apCamRParams[c] = camparams[c];
	}


	return;
}

Void cLocalizer::localize(cArray<cYUV<DepthPixelType>*> &apcYUVinputDepth, UInt uiVCam, cCfgParams cParams) {

	cMatrix<Double> PIP;
	PIP.init(4, 4);

	cMatrix<Double> PcIP;
	PcIP.init(4, 4);
	MatrixMultiply(m_apCamRParams[m_uiCenterCam]->m_mProjMat, m_apCamRParams[uiVCam]->m_mInvProjMat, PcIP);

	cMatrix<Double> pixDst(4, 1);

	Double dInvZfar = 1.0 / m_dZfar;
	Double dInvZnear = 1.0 / m_dZnear;

	MatrixMultiply(m_apCamRParams[uiVCam]->m_mProjMat, m_apCamRParams[uiVCam]->m_mInvProjMat, PIP);

	Double dSourceD, dTargetD, dTargetZ;
	Double dInvZ;
	Int iTargetH, iTargetW;

	Double tmpw[4], tmph[4], tmp1[4];

	for (Int i = 0; i < 4; i++) tmp1[i] = PIP.at(i, 2);
	for (Double h = 0; h < cParams.m_uiSourceHeight; h += 1) {
		for (Int i = 0; i < 4; i++) tmph[i] = h * PIP.at(i, 1) + tmp1[i];
		for (Double w = 0; w < cParams.m_uiSourceWidth; w += 1) {
			for (Int i = 0; i < 4; i++) tmpw[i] = w * PIP.at(i, 0) + tmph[i];

			UInt pp = Int(h)*cParams.m_uiSourceWidth + Int(w);

			dSourceD = Double(apcYUVinputDepth[uiVCam]->m_atY[pp]) / MAX_DEPTH;
			dInvZ = dSourceD * (dInvZnear - dInvZfar) + dInvZfar;

			dInvZ = (w*PcIP.at(2, 0) + h * PcIP.at(2, 1) + PcIP.at(2, 2)) / (1 / (dInvZ)-PcIP.at(2, 3)); //new

			for (Int i = 0; i < 4; i++) pixDst.at(i, 0) = dInvZ * PIP.at(i, 3) + tmpw[i];

			iTargetW = Int(pixDst.at(0, 0) / pixDst.at(2, 0) + 0.5);
			iTargetH = Int(pixDst.at(1, 0) / pixDst.at(2, 0) + 0.5);

			dTargetZ = 1.0 / (pixDst.at(3, 0) / pixDst.at(2, 0));
			dTargetD = -MAX_DEPTH * (dInvZfar*dTargetZ - 1) / ((dInvZnear - dInvZfar) * dTargetZ);

			if (Abs(dTargetZ) < Abs(m_dZnear)) dTargetD = MAX_DEPTH;
			if (dTargetD < 0) dTargetD = 0;


			apcYUVinputDepth[uiVCam]->m_atY[pp] = DepthPixelType(dTargetD);
		}
	}

	return;
}


#endif
